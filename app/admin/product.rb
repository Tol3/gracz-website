ActiveAdmin.register Product do
  menu false
  if ActiveRecord::Base.connection.table_exists? 'categories'
    filter :category
    filter :publish, :as => :select, :collection => [["Published",true],["Not Publish",false]]
  end

  scope :all
  scope :Simple_Product do |task|
    task.where('product_type = ?', "Simple Product")
  end
  scope :Classic_Product do |task|
    task.where('product_type = ?', "Classic Product")
  end
  scope :Other_Product do |task|
    task.where('product_type = ?', "Other Product")
  end

  index do
    selectable_column

    column "Picture", :product_images_id do |p|
      if ProductImage.find_by_product_id(p.id) != nil
        image_tag(ProductImage.find_by_product_id(p.id).image.url(:mini_thumb))
      else
        "No Picture"
      end
    end

    column "Model", :model

    column "Name", :sortable => :name do |p|
      link_to p.name, admin_product_path(p)
    end

    column "Product Type", :product_type

    column "Category", :sortable => :category_id do |c|
      if  c.category_id != nil
        c.category.name
      end
    end

    column "Publish", :publish do |p|
      status_tag (p.publish ? "Published" : "Not Publish"), (p.publish ? :ok : :error)
    end

    actions
  end

  show do
    panel "Details" do
      attributes_table_for resource do
        row("Picture") do
          ProductImage.find_all_by_product_id(resource.id).each do |a|
            text_node image_tag(a.image.url(:mini_thumb))
            text_node " "
          end
        end
        row("Type of Product") {resource.product_type}
        row("category") {resource.category}  
        row("model") { resource.model }
        row("name") { resource.name }
        row("Description") { resource.description }
        row("Weight") {"#{resource.weight} g."}

        row("Dimension") do |field|
          "#{field.dimension_x} X #{field.dimension_y} X #{field.dimension_z}"
        end

        row("Stock Keeping Unit") do
          StockUnit.find_all_by_product_id(resource.id).each_with_index do |a, index|
            text_node "Stock Keeping Unit => #{index + 1}"
            text_node " : </br>".html_safe
            text_node "#{a.pcsppack} pcs / pack</br>".html_safe
            text_node "#{a.packpctn} pack / ctn </br></br>".html_safe
            
          end
        end

        row("Publish") do
          if resource.publish == true
            "Published"
          elsif resource.publish == false
            "Not Publish"
          end
        end

      end
    end
  end

  form :html => {:multipart => true} do |f|

    f.globalize_inputs :translations do |lf|
        lf.inputs do          
          # :address, :day, :holiday, :fax, :name, :tel
          lf.input :name, :hint => "*** Can't be blank", :required => true
          lf.input :description, :hint => "*** Can't be blank", :required => true

          lf.input :locale, :as => :hidden
        end
      end

    f.inputs "Product Details" do
      f.input :product_type, :as => :radio, :label => "Type of product", :collection => ["Simple Product","Classic Product","Other Product"]
      f.input :category_id, as: :select, collection: Category.all,  :label => "Category :"#,:required => true
      f.input :model, :label => "Model :"#,:required => true
     
      f.input :dimension_x, :label => "Dimension :X(mm.)"#,:required => true#,:max_width => "50px", :min_width => "20px"
      f.input :dimension_y, :label => "Dimension :Y(mm.)"#,:required => true
      f.input :dimension_z, :label => "Dimension :Z(mm.)"#,:required => true
      f.input :weight, :label => "Weight (g.) :"#,:required => true
    end

    f.inputs "Product Stock Keeping Unit" do
      f.has_many :stock_units, :allow_destroy => true, :heading => 'Stock Keeping Unit' do |p|
        p.input :sku, :label => "SKU. :"
        p.input :pcsppack, :label => "pcs / pack :"
        p.input :packpctn, :label => "pack / ctn. :"
      end
    end

    f.inputs "Product Image" do
      f.has_many :product_images, :allow_destroy => true, :heading => 'Image Upload' do |p|
        p.input :image, :as => :file, :label => "Image"
      end
    end

    f.inputs "Publish Content" do
      f.input :publish, :as => :radio, :collection => {"Published" => true , "Not Publish" => false}, :input_html => { :checked => 'checked' }
    end

    f.actions
  end

end
