ActiveAdmin.register Event do
  menu false

  scope :all
  scope :publish do |task|
    task.where('publish = ?', true)
  end
  scope :not_publish do |task|
    task.where('publish = ?', false)
  end

  # menu :priority => 2
  index do
    selectable_column

    column "Cover", :cover do |p|
      if p.cover.url == nil  #"\/assets\/"
        "No Picture"
      else
        image_tag p.cover.url(:thumb)
      end 
    end

    column "Event", :sortable => :title do |p|
      link_to p.title, admin_event_path(p)
    end

    column "Publish", :publish do |p|
      status_tag (p.publish ? "Published" : "Not Publish"), (p.publish ? :ok : :error)
    end

    actions
  end

  show do
    panel "Details" do
      attributes_table_for resource do

        row("Cover") do
          if resource.cover.url == nil  #"\/assets\/"
            "No Picture"
          else
            image_tag(resource.cover.cover.url)
          end
        end

        row("Title") { resource.title }
        row("Location") { resource.locate }
        row("Content") { resource.content.html_safe}
        row("Tags") { resource.tag_list }
        row("Event Date") { resource.date }

        row("Publish") do
          if resource.publish == true
            "Published"
          elsif resource.publish == false
            "Not Publish"
          end
        end



      end
    end
  end

  form do |f|
    f.inputs "New Event" do
      f.input :cover, :as => :file, :label => "Cover Image"

      f.input :title

      f.input :locate      
    end
    f.inputs "Content" do
      f.input :teaser, :label => "Teaser Text"
      f.input :content, as: :wysihtml5, commands: :all, blocks: :all, :label => false
      
      f.input :tag_list, :input_html => { "data-role" => "tagsinput" }, :placeholder => "Add Tags", :label => "Tags"

       # f.input :tag_list, 
       #        :input_html => {"data-pre" => Tag.all.map(&:attributes).to_json }, 
       #        :placeholder => "Add Tags", 
       #        :label => "Tags"

      # f.input :tag_list, :as => :select,
      #                    :multiple => :true,
      #                    :collection => ActsAsTaggableOn::Tag.all.map(&:name)
    end
    f.inputs "Event Date" do
      f.input :date, :as => :just_datetime_picker
    end
    f.inputs "Publish Content" do
      f.input :publish, :as => :radio, :collection => {"Published" => true , "Not Publish" => false}, :input_html => { :checked => 'checked' }
    end
    f.buttons
  end

  controller do

      def events
        @tags = Tag.where("name like ?", "%#{params[:q]}%")

        respond_to do |format|
          format.html
          format.json { render :json => @tags.map(&:attributes) }
        end

      end

  end

end
