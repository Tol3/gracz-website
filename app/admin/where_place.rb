ActiveAdmin.register WherePlace do
menu false
	index do
    selectable_column

    column "Logo", :logo do |p|
      if p.logo.url == nil  #"\/assets\/"
        "No Picture"
      else
        image_tag(p.logo.logo.url)
      end
    end

    column "Name", :sortable => :name do |p|
      link_to p.name, admin_where_place_path(p)
    end
    column "Link", :link do |p|
      p.link
    end
    
    actions
  end

end
