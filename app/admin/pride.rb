ActiveAdmin.register Pride do
	menu false

  scope :all
  scope :publish do |task|
    task.where('published = ?', true)
  end
  scope :not_publish do |task|
    task.where('published = ?', false)
  end


  form do |f|
    f.inputs "Image Pride" do
      f.input :image, :as => :file, :label => "Pride Picture"
    end

    f.inputs "Content" do
      f.input :category, :label => "Type", as: :radio, collection: ["Awards", "Certificates", "Test Reports"]
    end

    f.globalize_inputs :translations do |lf|
        lf.inputs do          
          lf.input :description, :label => "Description"

          lf.input :locale, :as => :hidden
        end
      end

    f.inputs "Publish Content" do
      f.input :published
    end

    f.buttons
  end



	index do
	    selectable_column

	    column "Image", :image do |p|
	      if p.image.url == nil  #"\/assets\/"
	        "No Picture"
	      else
	        image_tag p.image.url(:thumb)
	      end 
	    end

      column "Type", :sortable => :category do |p|
        p.category
      end

      column "Description", :sortable => :description do |p|
        link_to p.description.truncate(60), admin_pride_path(p)
      end

	    column "Publish", :published do |p|
	      status_tag (p.published ? "Published" : "Not Publish"), (p.published ? :ok : :error)
	    end
	    actions
	end

	show do
    panel "Details" do
      attributes_table_for resource do

        row("Cover") do
          if resource.image.url == nil  #"\/assets\/"
            "No Picture"
          else
            image_tag(resource.image.thumb.url)
          end
        end

        row("Description(EN.)") { resource.description(:en) }
        row("Description(TH.)") { resource.description(:th) }
        row("Type") { resource.category }

        row("Publish") do
          if resource.published == true
            "Published"
          elsif resource.published == false
            "Not Publish"
          end
        end

      end
    end
  end

end
