ActiveAdmin.register Category do
  menu false

# :name, :simple, :classic, :other

  	form do |f|
	    f.inputs "Category" do
	    	f.input :simple
	    	f.input :classic
	    	f.input :other
		end

	    f.globalize_inputs :translations do |lf|
	      lf.inputs do	      	
	      	# :address, :day, :holiday, :fax, :name, :tel
	        lf.input :name

	        lf.input :locale, :as => :hidden
	      end
	    end

	    f.buttons
	end

	show do
    panel "Details" do
      attributes_table_for resource do

        row("Name(EN.)") { resource.name(:en) }
        row("Name(TH.)") { resource.name(:th) }

        row("Simple") { resource.simple }
        row("Classic") { resource.classic }
        row("Other") { resource.other }

      end
    end
  end
end
