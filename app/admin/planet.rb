ActiveAdmin.register Planet do
  menu false

  scope :all
  scope :publish do |task|
    task.where('publish = ?', true)
  end
  scope :not_publish do |task|
    task.where('publish = ?', false)
  end
  
  index do
    selectable_column

    column "Cover", :cover do |p|
      if p.cover.url == nil  #"\/assets\/"
        "No Picture"
      else
        image_tag(p.cover.mini_cover.url)
      end  
    end

    column "Planet", :sortable => :title do |p|
      link_to p.title, admin_planet_path(p)
    end

    column "Publish Status", :publish do |p|
      status_tag (p.publish ? "Published" : "Not Publish"), (p.publish ? :ok : :error)
    end

    actions
  end

  show do
    panel "Details" do
      attributes_table_for resource do
        row("Cover First") do
          if resource.cover.url == nil  #"\/assets\/"
            "No Picture"
          else
            image_tag(resource.cover.cover.url)
          end
        end

        row("Cover Second") do
          if resource.cover_two.url == nil  #"\/assets\/"
            "No Picture"
          else
            image_tag(resource.cover_two.cover.url)
          end
        end

        row("Title") { resource.title }
        row("Content") { resource.content.html_safe}
        row("Tags") { resource.tag_list }

        row("Publish") do
          if resource.publish == true
            "Published"
          elsif resource.publish == false
            "Not Publish"
          end
        end

      end
    end
  end

  form do |f|
    f.inputs "New Planet" do
      f.input :cover, :as => :file, :label => "Cover First"
      f.input :cover_two, :as => :file, :label => "Cover Second"

      f.input :title
    end
    f.inputs "Content" do
      f.input :teaser, :label => "Teaser Text"
      f.input :content, as: :wysihtml5, commands: :all, blocks: :all, :label => false
      f.input :tag_list, :input_html => { "data-role" => "tagsinput" }, :placeholder => "Add Tags", :label => "Tags"
    end

    f.inputs "Publish Content" do
      f.input :publish, :as => :radio, :collection => {"Published" => true , "Not Publish" => false}, :input_html => { :checked => 'checked' }
    end

    f.buttons
  end
end
