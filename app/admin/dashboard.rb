ActiveAdmin.register_page "Dashboard" do

  menu :priority => 1, :label => "Dashboard"

  # menu :parent => "Super Admin Only", :if => proc { current_admin_user.super_admin? }

  content :title => "Dashboard" do
    # div :class => "blank_slate_container", :id => "dashboard_default_message" do
    #   span :class => "blank_slate" do
    #     span I18n.t("active_admin.dashboard_welcome.welcome")
    #     small I18n.t("active_admin.dashboard_welcome.call_to_action")
    #   end
    # end

    # Here is an example of a simple dashboard with columns and panels.
    #
    columns do
      column do
        panel "Recent Planet" do
          ul do
            Planet.limit(5).map do |post|
              li "Title #{link_to(post.title, admin_planet_path(post))} </br>Status: #{post.publish} ".html_safe
            end
          end
        end
      end


      column do
        panel "Recent Life" do
          ul do
            Life.last(5).map do |post|
              li "Title #{link_to(post.title, admin_life_path(post))} </br>Status: #{post.publish}".html_safe
            end
          end
        end
      end

      column do
        panel "Recent Events" do
          ul do
            Event.last(5).map do |post|
              li "Title #{link_to(post.title, admin_event_path(post))} </br>Status: #{post.publish}".html_safe
            end
          end
        end
      end
    end

    columns do
      column do
        panel "Recent Product" do
          table_for Product.last(5).reverse.map do |item|
            column("model")   { |item| item.model }
            column("name")   { |item| link_to(item.name, admin_product_path(item)) }
            column("category")   { |item| if item.category_id != nil
              item.category.name
              end
            }
            column("type")   { |item| item.product_type }
            column("status")   { |item| item.publish }
          end
        end
      end
    end

  end # content
end
