ActiveAdmin.register WhereCategory do
menu false

form do |f|

	    f.globalize_inputs :translations do |lf|
	      lf.inputs do	      	
	      	# :address, :day, :holiday, :fax, :name, :tel
	        lf.input :name

	        lf.input :locale, :as => :hidden
	      end
	    end

	    f.buttons
	end

	show do
    panel "Details" do
      attributes_table_for resource do

        row("Name(EN.)") { resource.name(:en) }
        row("Name(TH.)") { resource.name(:th) }

      end
    end
  end
end
