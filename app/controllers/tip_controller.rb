class TipController < ApplicationController
	def index
		@tips = Tip.where(publish: true).reverse_order.page(params[:page]).per(15)
		@meta = {:title => 'Gracz Tips',
             :type => 'website', 
             :url => request.url, 
             :image => 'http://107.170.202.81:9292'+"#{@tips.first.cover.thumb.url}", 
             :description => 'Gracz Lorem'}
	end
end
