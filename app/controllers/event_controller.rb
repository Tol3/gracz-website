class EventController < ApplicationController

  def index
  	if params[:year]
  		z = params[:year]
  		@events = Event.where("date >= :start_date AND date <= :end_date",{start_date: Time.zone.parse("#{z}-01-01 00:00:00 UTC"), end_date: Time.zone.parse("#{z}-12-31 00:00:00 UTC")}).reverse_order.page(params[:page]).per(8)
    else
   		@events = Event.where(publish: true).reverse_order.page(params[:page]).per(8)
   	end

   	i = Time.now.year.to_i

   	@years = []

   	10.times do |l|
   		@years << i-l
   	end

    if @events != []
    	@meta = {:title => @events.first.title,
               :type => 'website', 
               :url => request.url, 
               :image => 'http://107.170.202.81:9292'+"#{@events.first.cover.thumb.url}", 
               :description => @events.first.teaser}
    else
      @meta = {:title =>'Gracz',
             :type => 'website', 
             :url => request.url, 
             :image => 'http://107.170.202.81:9292', 
             :description => 'Gracz Event'}
    end
  end

  def show
    @event = Event.find(params[:id])
  	@meta = {:title => @event.title,
             :type => 'website', 
             :url => request.url, 
             :image => 'http://107.170.202.81:9292'+"#{@event.cover.thumb.url}", 
             :description => @event.teaser}
  end
end