class LifeController < ApplicationController
  def index
  	if Planet.where(publish: true) != nil
	  	@first = Life.where(publish: true).reverse_order.first
	  	# @life
	  	@contents = Life.where(publish: true).reverse_order.page(params[:page]).per(8)
	  	# if @contents != nil
	  	# 	@contents.shift
	  	# end
  	end

    @meta = {:title => @first.teaser,
             :type => 'website', 
             :url => request.url, 
             :image => 'http://107.170.202.81:9292'+"#{@first.cover.thumb.url}", 
             :description => @first.teaser}
  end

  def show
    @content = Life.find(params[:id])
    @meta = {:title => @content.title,
             :type => 'website', 
             :url => request.url, 
             :image => 'http://107.170.202.81:9292'+"#{@content.cover.thumb.url}", 
             :description => @content.teaser}
  end
end
