class HomeController < ApplicationController
  def index

    # product = Product.where(publish: true)
    # @products = product[0..11]
    @meta = {:title => 'Gracz',
             :type => 'website',
             :url => request.url,
             :image => 'http://107.170.202.81:9292/assets/Gracz_Logo_02-0eb55ef7043a917cf6b745d85097c94e.png',
             :description => 'ภาชนะเกรซ คือ ภาชนะ เพื่อชีวิตที่ยืนยาว ผลิตจากเยื้อพันธุ์พืชบริสุทธิ์ ผ่านกระบวนการที่ทันสมัย ผ่านการรับรองมาตราฐานระดับนานาชาติ เพื่อให้ผลิตภัณฑ์ที่ดีที่สุดสําหรับตัวคุณ เพราะเราเชื่อว่ามนุษย์มีเพียงชีวิตเดียว และอยู่บนโลกใบเดียว เราจึงตั้งมั่นที่จะทําทุกอย่าง เพื่อดูแลทุกคนบนโลก ใบนี้ ให้มีชีวิตที่ยืนยาวและมั่นคงตลอดไป'}
  end

  def contact
    @meta = {:title => 'Gracz contact',
             :type => 'website',
             :url => request.url,
             :image => 'http://107.170.202.81:9292/assets/Gracz_Logo_02-0eb55ef7043a917cf6b745d85097c94e.png',
             :description => '46/151 Nuan Chan Khwaeng Nuanchan, Khet Bueng Kum Krungthep Mahanakorn 10230'}
  end

  def find_us
    @category = WhereCategory.all
    @meta = {:title => 'Gracz find us',
             :type => 'website',
             :url => request.url,
             :image => 'http://107.170.202.81:9292/assets/Gracz_Logo_02-0eb55ef7043a917cf6b745d85097c94e.png',
             :description => '46/151 Nuan Chan Khwaeng Nuanchan, Khet Bueng Kum Krungthep Mahanakorn 10230'}
  end

  def story
    @meta = {:title => 'Gracz story',
             :type => 'website',
             :url => request.url,
             :image => 'http://107.170.202.81:9292/assets/Gracz_Logo_02-0eb55ef7043a917cf6b745d85097c94e.png',
             :description => 'Gracz Lorem'}
  end

  def tagged
    @meta = {:title => 'Gracz Tagged',
             :type => 'website',
             :url => request.url,
             :image => 'http://107.170.202.81:9292/assets/Gracz_Logo_02-0eb55ef7043a917cf6b745d85097c94e.png',
             :description => 'Gracz Lorem'}

    if params[:tag]
      @tags = Event.where(publish: true).reverse_order.tagged_with(params[:tag])|
            Planet.where(publish: true).reverse_order.tagged_with(params[:tag])|
            Life.where(publish: true).reverse_order.tagged_with(params[:tag])|
            Tip.where(publish: true).reverse_order.tagged_with(params[:tag])

      tag = Event.where(publish: true).reverse_order.tagged_with(params[:tag])|
            Planet.where(publish: true).reverse_order.tagged_with(params[:tag])|
            Life.where(publish: true).reverse_order.tagged_with(params[:tag])|
            Tip.where(publish: true).reverse_order.tagged_with(params[:tag])

      @t = Kaminari.paginate_array(tag).page(params[:page]).per(10)
    else
      redirect_to root_path
    end
  end

  def faq
    @meta = {:title => 'Gracz FAQ',
             :type => 'website',
             :url => request.url,
             :image => 'http://107.170.202.81:9292/assets/Gracz_Logo_02-0eb55ef7043a917cf6b745d85097c94e.png',
             :description => 'Gracz FAQ'}
  end


end
















