class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :check_browser
  before_filter :set_locale
  helper_method :current_user

  def picture_from_url(url)
    self.picture = open(url)
  end


  def local_request?
    false
  end

  def rescue_action_in_public(exception)
    case exception
    when ActiveRecord::RecordNotFound
      render :file => "#{RAILS_ROOT}/public/404.html", :status => 404
    else
      super
    end
  end

  private
  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
    Rails.application.routes.default_url_options[:locale]= I18n.locale
  end

  private
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  private
  Browser = Struct.new(:browser, :version)

  SupportedBrowsers = [
    Browser.new('Safari', '5.1'),
    Browser.new('Firefox', '17.0.0'),
    Browser.new('Internet Explorer', '9.0'),
    Browser.new('Opera', '11.50'),
    Browser.new('Chrome', '25.0.1364')
  ]

  def check_browser
    user_agent = UserAgent.parse(request.user_agent)

    unless SupportedBrowsers.detect { |browser| user_agent >= browser }

      # render :file => "home/notsupport.html.erb", :layout => false

    end

  end

  # Override build_footer method in ActiveAdmin::Views::Pages
  # require 'active_admin_views_pages_base.rb'
end
