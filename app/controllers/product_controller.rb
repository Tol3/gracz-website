class ProductController < ApplicationController

  def index
    @meta = {:title => 'Gracz Product',
             :type => 'website', 
             :url => request.url, 
             :image => 'http://107.170.202.81:9292/assets/Gracz_Logo_02-0eb55ef7043a917cf6b745d85097c94e.png', 
             :description => 'Gracz Product'}
  end

  def classic

    t = params[:type].to_i
    
    if(params[:type])
      Category.all.each do |i|
        if (t <= (Category.count+1)) && (t > 0)
          @products = Product.where("product_type = ? AND publish = ? AND category_id = ?", "Classic Product", true, "#{t}" ).reverse_order.page(params[:page]).per(15)
        else
          @products = Product.where("product_type = ? AND publish = ?", "Classic Product", true ).reverse_order.page(params[:page]).per(15)
        end
      end
    else
      @products = Product.where("product_type = ? AND publish = ?", "Classic Product", true ).reverse_order.page(params[:page]).per(15)
    end

    @meta = {:title => @products.first.name,
             :type => 'website', 
             :url => request.url, 
             :image => 'http://107.170.202.81:9292/assets/Gracz_Logo_02-0eb55ef7043a917cf6b745d85097c94e.png', 
             :description => @products.first.description}
  end

  def simple
    
    t = params[:type].to_i
    if(params[:type])
      Category.all.each do |i|
        if (t <= (Category.count+1)) && (t > 0)
          @products = Product.where("product_type = ? AND publish = ? AND category_id = ?", "Simple Product", true, "#{t}" ).reverse_order.page(params[:page]).per(15)
        else
          @products = Product.where("product_type = ? AND publish = ?", "Simple Product", true ).reverse_order.page(params[:page]).per(15)
        end
      end
    else
      @products = Product.where("product_type = ? AND publish = ?", "Simple Product", true ).reverse_order.page(params[:page]).per(15)
    end
    @meta = {:title => @products.first.name,
             :type => 'website', 
             :url => request.url, 
             :image => 'http://107.170.202.81:9292/assets/Gracz_Logo_02-0eb55ef7043a917cf6b745d85097c94e.png', 
             :description => @products.first.description}
  end

  def other
    t = params[:type].to_i
    if(params[:type])
      Category.all.each do |i|
        if (t <= (Category.count+1)) && (t > 0)
          @products = Product.where("product_type = ? AND publish = ? AND category_id = ?", "Other Product", true, "#{t}" ).reverse_order.page(params[:page]).per(15)
        else
          @products = Product.where("product_type = ? AND publish = ?", "Other Product", true ).reverse_order.page(params[:page]).per(15)
        end
      end
    else
      @products = Product.where("product_type = ? AND publish = ?", "Other Product", true ).reverse_order.page(params[:page]).per(15)
    end
    @meta = {:title => 'Other Product',
             :type => 'website', 
             :url => request.url, 
             :image => 'http://107.170.202.81:9292/assets/Gracz_Logo_02-0eb55ef7043a917cf6b745d85097c94e.png', 
             :description => 'Other Product'}
  end

  def show
    @product = Product.find(params[:id])
    @meta = {:title => @product.name,
             :type => 'website', 
             :url => request.url, 
             :image => 'http://107.170.202.81:9292' +'#{ProductImage.find(@product.id).image.thumb.url}',
             :description => @product.description}
  end

end
