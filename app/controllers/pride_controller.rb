class PrideController < ApplicationController
  def index
	@pride = Pride.where(published: true).reverse_order

	@awd = Pride.where(published: true, category: "Awards" ).reverse_order
	@cer = Pride.where(published: true, category: "Certificates" ).reverse_order
	@trp = Pride.where(published: true, category: "Test Reports" ).reverse_order
	# .page(params[:page]).per(9)
	@meta = {:title => 'Gracz Pride',
             :type => 'website', 
             :url => request.url, 
             :image => 'http://107.170.202.81:9292/assets/Gracz_Logo_02-0eb55ef7043a917cf6b745d85097c94e.png', 
             :description => 'Gracz Pride'}
  end
end
