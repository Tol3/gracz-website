class PlanetController < ApplicationController
  def index
    if Planet.where(publish: true) != nil
      @first = Planet.where(publish: true).reverse_order.first
      # @life
      @contents = Planet.where(publish: true).reverse_order.page(params[:page]).per(8)
      # if @contents != nil
      #   @contents.shift
      # end
    end
    @meta = {:title => @first.title,
             :type => 'website', 
             :url => request.url, 
             :image => 'http://107.170.202.81:9292'+"#{@first.cover.thumb.url}", 
             :description => @first.teaser}
  end

  def show
    @content = Planet.find(params[:id])
    @meta = {:title => @content.title,
             :type => 'website', 
             :url => request.url, 
             :image => 'http://107.170.202.81:9292'+"#{@content.cover.thumb.url}", 
             :description => @content.teaser}
  end

 #  def index
 #  	if params[:tag]
	# 	# @events = Event.joins(planet: :life)
	# 	# @events = Event.tagged_with(params[:tag]).reverse_order.page(params[:page]).per(2)
	# else
 #    	@planets = Planet.where(publish: true).reverse_order.page(params[:page]).per(2)
 #    end
 #  end

 #  def show
 #    @planet = Planet.find(params[:id])
 #  end
end
