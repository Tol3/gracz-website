class Tip < ActiveRecord::Base
  attr_accessible :content, :cover, :title, :publish, :tag_list
  has_many :taggings
  has_many :tags, through: :taggings

  mount_uploader :cover, CoverUploader
   validates :title, :presence => true

   def self.tagged_with(name)
    Tag.find_by_name!(name).tips
  end

  def self.tag_counts
    Tag.select("tags.*, count(taggings.tag_id) as count").
      joins(:taggings).group("taggings.tag_id")
  end

  def tag_list
    tags.map(&:name).join(", ")
  end

  def tag_list=(names)
    self.tags = names.split(",").map do |n|
      Tag.where(name: n.strip).first_or_create!
    end
  end
end
