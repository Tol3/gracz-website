class WherePlace < ActiveRecord::Base
  attr_accessible :link, :logo, :name, :where_category_id
  belongs_to :where_category

  validates :where_category_id, :presence => true

  mount_uploader :logo, LogoUploader
end

 # :logo,