class Category < ActiveRecord::Base
  attr_accessible :name, :simple, :classic, :other, :translations, :translations_attributes
  has_many :products

  translates :name
  accepts_nested_attributes_for :translations

  def self.to_s
    self.name
  end
end
