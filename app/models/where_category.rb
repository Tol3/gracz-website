class WhereCategory < ActiveRecord::Base
  attr_accessible :name, :translations, :translations_attributes
  has_many :where_places

  validates :name, :presence => true

  translates :name
  accepts_nested_attributes_for :translations

  def self.to_s
    self.name
  end
end
