class Tag < ActiveRecord::Base
  attr_accessible :name

  has_many :taggings
  
  has_many :events, through: :taggings
  has_many :tips, through: :taggings
  has_many :lifes, through: :taggings
  has_many :planets, through: :taggings

end
