class StockUnit < ActiveRecord::Base
  attr_accessible :packpctn, :pcsppack, :product_id, :sku

  belongs_to :product

  validates :packpctn, :pcsppack, :presence => true
end
