class Pride < ActiveRecord::Base
  attr_accessible :description, :image, :published, :category, :translations, :translations_attributes

  mount_uploader :image, CoverUploader
  validates :description,:category, :presence => true
  translates :description
  accepts_nested_attributes_for :translations
end
