class Product < ActiveRecord::Base
  attr_accessible :category_id, :description, :dimension_x, :dimension_y, :dimension_z, :model, :name, :weight, :product_images_attributes, :publish, :product_type,:stock_units_attributes, :translations, :translations_attributes
  has_many :product_images
  has_many :stock_units
  belongs_to :category

  validates :dimension_x,:dimension_y,:dimension_z, :numericality => { :greater_than_or_equal_to => 0.00 , :message => "Dimension must be number only"}
  validates :weight,:numericality => { :greater_than_or_equal_to => 0.00 , :message => "must be number only"}
  validates :product_type, :description, :model, :name, :presence => true

  accepts_nested_attributes_for :product_images,:stock_units,allow_destroy: true

  translates :name, :description
  accepts_nested_attributes_for :translations


  def self.to_s
    self.name
  end
end
