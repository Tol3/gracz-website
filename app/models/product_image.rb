class ProductImage < ActiveRecord::Base
  attr_accessible :image, :product_id
  belongs_to :product

  validates :image, :presence => true
  mount_uploader :image, ProductImageUploader
  # image_accessor :image
end
