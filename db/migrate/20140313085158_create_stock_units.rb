class CreateStockUnits < ActiveRecord::Migration
  def change
    create_table :stock_units do |t|
      t.integer :pcsppack
      t.integer :packpctn
      t.integer :product_id

      t.timestamps
    end
  end
end
