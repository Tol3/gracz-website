class AddPublishToTable < ActiveRecord::Migration
  def change
  	add_column :events, :publish, :boolean
  	add_column :lives, :publish, :boolean
  	add_column :planets, :publish, :boolean
  	add_column :products, :publish, :boolean
  end
end
