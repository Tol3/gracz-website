class AddTeaserToTable < ActiveRecord::Migration
  def change
  	add_column :lives, :teaser, :string, limit: 255
  	add_column :events, :teaser, :string, limit: 255
  	add_column :planets, :teaser, :string, limit: 255
  end
end
