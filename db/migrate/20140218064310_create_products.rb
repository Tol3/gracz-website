class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.string :model
      t.text :description
      t.integer :category_id
      t.float :dimension_x
      t.float :dimension_y
      t.float :dimension_z
      t.float :weight
      t.integer :pcsppack
      t.integer :packpctn

      t.timestamps
    end
  end
end
