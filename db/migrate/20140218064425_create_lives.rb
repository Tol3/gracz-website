class CreateLives < ActiveRecord::Migration
  def change
    create_table :lives do |t|
      t.string :cover
      t.string :title
      t.text :content

      t.timestamps
    end
  end
end
