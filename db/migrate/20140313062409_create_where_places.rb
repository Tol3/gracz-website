class CreateWherePlaces < ActiveRecord::Migration
  def change
    create_table :where_places do |t|
      t.string :logo
      t.string :name
      t.string :link

      t.timestamps
    end
  end
end
