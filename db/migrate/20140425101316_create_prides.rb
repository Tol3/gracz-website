class CreatePrides < ActiveRecord::Migration
  def change
    create_table :prides do |t|
      t.string :image
      t.string :description

      t.timestamps
    end
  end
end
