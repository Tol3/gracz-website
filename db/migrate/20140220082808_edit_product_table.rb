class EditProductTable < ActiveRecord::Migration
  def change
    # add_column :products, :type, :string
    rename_column :products, :type, :product_type
  end
end
