class AddColumnToCategory < ActiveRecord::Migration
  def change
  	add_column :categories, :classic, :boolean
  	add_column :categories, :simple, :boolean
  	add_column :categories, :other, :boolean
  end
end