class CreateTips < ActiveRecord::Migration
  def change
    create_table :tips do |t|
      t.string :cover
      t.string :title
      t.text :content
      t.boolean :publish

      t.timestamps
    end
  end
end
