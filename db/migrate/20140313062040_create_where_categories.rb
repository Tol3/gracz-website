class CreateWhereCategories < ActiveRecord::Migration
  def change
    create_table :where_categories do |t|
      t.string :name

      t.timestamps
    end
  end
end
