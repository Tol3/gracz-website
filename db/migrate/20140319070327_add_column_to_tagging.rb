class AddColumnToTagging < ActiveRecord::Migration
  def change
  	add_column :taggings, :tip_id, :integer
  	add_column :taggings, :life_id, :integer
  	add_column :taggings, :planet_id, :integer
  end
end
