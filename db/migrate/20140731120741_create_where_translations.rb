class CreateWhereTranslations < ActiveRecord::Migration
  def self.up
    WhereCategory.create_translation_table!({
      :name => :string
    }, {
      :migrate_data => true
    })
  end

  def self.down
    WhereCategory.drop_translation_table! :migrate_data => true
  end
end
