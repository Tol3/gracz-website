class CreatePrideTranslations < ActiveRecord::Migration
  def self.up
    Pride.create_translation_table!({
      :description => :string
    }, {
      :migrate_data => true
    })
  end

  def self.down
    Pride.drop_translation_table! :migrate_data => true
  end
end
