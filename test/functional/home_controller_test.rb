require 'test_helper'

class HomeControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get contact" do
    get :contact
    assert_response :success
  end

  test "should get find_us" do
    get :find_us
    assert_response :success
  end

  test "should get pride" do
    get :pride
    assert_response :success
  end

  test "should get story" do
    get :story
    assert_response :success
  end

end
