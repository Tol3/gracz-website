U:RDoc::AnyMethod[iI"as_json:EFI"+ActiveModel::Serializers::JSON#as_json;FF:publico:RDoc::Markup::Document:@parts[o:RDoc::Markup::Paragraph;	[I"FReturns a hash representing the model. Some configuration can be ;TI"passed through +options+.;To:RDoc::Markup::BlankLine o;
;	[I"NThe option <tt>include_root_in_json</tt> controls the top-level behavior ;TI"Kof +as_json+. If true (the default) +as_json+ will emit a single root ;TI"5node named after the object's type. For example:;T@o:RDoc::Markup::Verbatim;	[I"user = User.find(1)
;TI"user.as_json
;TI"A# => { "user": {"id": 1, "name": "Konata Izumi", "age": 16,
;TI"D                "created_at": "2006/08/01", "awesome": true} }
;TI"
;TI"5ActiveRecord::Base.include_root_in_json = false
;TI"user.as_json
;TI"7# => {"id": 1, "name": "Konata Izumi", "age": 16,
;TI"8      "created_at": "2006/08/01", "awesome": true}
;T:@format0o;
;	[I"^This behavior can also be achieved by setting the <tt>:root</tt> option to +false+ as in:;T@o;;	[	I"user = User.find(1)
;TI"user.as_json(root: false)
;TI"8# =>  {"id": 1, "name": "Konata Izumi", "age": 16,
;TI"9       "created_at": "2006/08/01", "awesome": true}
;T;0o;
;	[I"YThe remainder of the examples in this section assume include_root_in_json is set to ;TI"<tt>false</tt>.;T@o;
;	[I"KWithout any +options+, the returned Hash will include all the model's ;TI"attributes. For example:;T@o;;	[	I"user = User.find(1)
;TI"user.as_json
;TI"7# => {"id": 1, "name": "Konata Izumi", "age": 16,
;TI"8      "created_at": "2006/08/01", "awesome": true}
;T;0o;
;	[I"YThe <tt>:only</tt> and <tt>:except</tt> options can be used to limit the attributes ;TI"Hincluded, and work similar to the +attributes+ method. For example:;T@o;;	[
I"+user.as_json(:only => [ :id, :name ])
;TI",# => {"id": 1, "name": "Konata Izumi"}
;TI"
;TI"9user.as_json(:except => [ :id, :created_at, :age ])
;TI"4# => {"name": "Konata Izumi", "awesome": true}
;T;0o;
;	[I"STo include the result of some method calls on the model use <tt>:methods</tt>:;T@o;;	[	I"*user.as_json(:methods => :permalink)
;TI"7# => {"id": 1, "name": "Konata Izumi", "age": 16,
;TI"8      "created_at": "2006/08/01", "awesome": true,
;TI"*      "permalink": "1-konata-izumi"}
;T;0o;
;	[I"3To include associations use <tt>:include</tt>:;T@o;;	[
I"&user.as_json(:include => :posts)
;TI"7# => {"id": 1, "name": "Konata Izumi", "age": 16,
;TI"8      "created_at": "2006/08/01", "awesome": true,
;TI"R      "posts": [{"id": 1, "author_id": 1, "title": "Welcome to the weblog"},
;TI"M                {"id": 2, author_id: 1, "title": "So I was thinking"}]}
;T;0o;
;	[I"=Second level and higher order associations work as well:;T@o;;	[I",user.as_json(:include => { :posts => {
;TI"A                               :include => { :comments => {
;TI"F                                             :only => :body } },
;TI"9                               :only => :title } })
;TI"7# => {"id": 1, "name": "Konata Izumi", "age": 16,
;TI"8      "created_at": "2006/08/01", "awesome": true,
;TI"P      "posts": [{"comments": [{"body": "1st post!"}, {"body": "Second!"}],
;TI"9                 "title": "Welcome to the weblog"},
;TI"F                {"comments": [{"body": "Don't think too hard"}],
;TI"5                 "title": "So I was thinking"}]};T;0:
@fileI")lib/active_model/serializers/json.rb;T:0@omit_headings_from_table_of_contents_below000[ I"(options = nil);T@dFI"	JSON;FcRDoc::NormalModule0