U:RDoc::AnyMethod[iI"has_secure_password:EFI"BActiveModel::SecurePassword::ClassMethods#has_secure_password;FF:publico:RDoc::Markup::Document:@parts[o:RDoc::Markup::Paragraph;	[I"EAdds methods to set and authenticate against a BCrypt password. ;TI"EThis mechanism requires you to have a password_digest attribute.;To:RDoc::Markup::BlankLine o;
;	[I"KValidations for presence of password, confirmation of password (using ;TI"Ca "password_confirmation" attribute) are automatically added. ;TI"5You can add more validations by hand if need be.;T@o;
;	[I"RYou need to add bcrypt-ruby (~> 3.0.0) to Gemfile to use has_secure_password:;T@o:RDoc::Markup::Verbatim;	[I"#gem 'bcrypt-ruby', '~> 3.0.0'
;T:@format0o;
;	[I"\Example using Active Record (which automatically includes ActiveModel::SecurePassword):;T@o;;	[I"9# Schema: User(name:string, password_digest:string)
;TI"%class User < ActiveRecord::Base
;TI"  has_secure_password
;TI"	end
;TI"
;TI"]user = User.new(:name => "david", :password => "", :password_confirmation => "nomatch")
;TI"buser.save                                                      # => false, password required
;TI"%user.password = "mUc3m00RsqyRe"
;TI"kuser.save                                                      # => false, confirmation doesn't match
;TI"2user.password_confirmation = "mUc3m00RsqyRe"
;TI"Nuser.save                                                      # => true
;TI"Ouser.authenticate("notright")                                  # => false
;TI"Nuser.authenticate("mUc3m00RsqyRe")                             # => user
;TI"MUser.find_by_name("david").try(:authenticate, "notright")      # => nil
;TI"MUser.find_by_name("david").try(:authenticate, "mUc3m00RsqyRe") # => user;T;0:
@fileI"(lib/active_model/secure_password.rb;T:0@omit_headings_from_table_of_contents_below000[ I"();T@/FI"ClassMethods;FcRDoc::NormalModule0