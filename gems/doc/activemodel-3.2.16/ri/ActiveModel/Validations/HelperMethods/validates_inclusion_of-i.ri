U:RDoc::AnyMethod[iI"validates_inclusion_of:EFI"CActiveModel::Validations::HelperMethods#validates_inclusion_of;FF:publico:RDoc::Markup::Document:@parts[
o:RDoc::Markup::Paragraph;	[I"NValidates whether the value of the specified attribute is available in a ;TI""particular enumerable object.;To:RDoc::Markup::BlankLine o:RDoc::Markup::Verbatim;	[I"'class Person < ActiveRecord::Base
;TI"8  validates_inclusion_of :gender, :in => %w( m f )
;TI"1  validates_inclusion_of :age, :in => 0..99
;TI"~  validates_inclusion_of :format, :in => %w( jpg gif png ), :message => "extension %{value} is not included in the list"
;TI"X  validates_inclusion_of :states, :in => lambda{ |person| STATES[person.country] }
;TI"	end
;T:@format0o;
;	[I"Configuration options:;To:RDoc::Markup::List:
@type:BULLET:@items[o:RDoc::Markup::ListItem:@label0;	[o;
;	[	I"I<tt>:in</tt> - An enumerable object of available items. This can be ;TI"Qsupplied as a proc or lambda which returns an enumerable. If the enumerable ;TI"Ais a range the test is performed with <tt>Range#cover?</tt> ;TI"N(backported in Active Support for 1.8), otherwise with <tt>include?</tt>.;To;;0;	[o;
;	[I"<<tt>:within</tt> - A synonym(or alias) for <tt>:in</tt>;To;;0;	[o;
;	[I"O<tt>:message</tt> - Specifies a custom error message (default is: "is not ;TI"included in the list").;To;;0;	[o;
;	[I"R<tt>:allow_nil</tt> - If set to true, skips this validation if the attribute ;TI"#is +nil+ (default is +false+).;To;;0;	[o;
;	[I"J<tt>:allow_blank</tt> - If set to true, skips this validation if the ;TI"-attribute is blank (default is +false+).;To;;0;	[o;
;	[I"J<tt>:on</tt> - Specifies when this validation is active. Runs in all ;TI"Pvalidation contexts by default (+nil+), other options are <tt>:create</tt> ;TI"and <tt>:update</tt>.;To;;0;	[o;
;	[	I"O<tt>:if</tt> - Specifies a method, proc or string to call to determine if ;TI"Mthe validation should occur (e.g. <tt>:if => :allow_validation</tt>, or ;TI"Q<tt>:if => Proc.new { |user| user.signup_step > 2 }</tt>). The method, proc ;TI"Bor string should return or evaluate to a true or false value.;To;;0;	[o;
;	[	I"P<tt>:unless</tt> - Specifies a method, proc or string to call to determine ;TI"Tif the validation should not occur (e.g. <tt>:unless => :skip_validation</tt>, ;TI"Tor <tt>:unless => Proc.new { |user| user.signup_step <= 2 }</tt>). The method, ;TI"Gproc or string should return or evaluate to a true or false value.;To;;0;	[o;
;	[I"G<tt>:strict</tt> - Specifies whether validation should be strict. ;TI"JSee <tt>ActiveModel::Validation#validates!</tt> for more information.;T:
@fileI".lib/active_model/validations/inclusion.rb;T:0@omit_headings_from_table_of_contents_below000[ I"(*attr_names);T@YFI"HelperMethods;FcRDoc::NormalModule0