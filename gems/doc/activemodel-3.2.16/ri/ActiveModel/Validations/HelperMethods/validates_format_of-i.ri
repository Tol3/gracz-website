U:RDoc::AnyMethod[iI"validates_format_of:EFI"@ActiveModel::Validations::HelperMethods#validates_format_of;FF:publico:RDoc::Markup::Document:@parts[o:RDoc::Markup::Paragraph;	[I"TValidates whether the value of the specified attribute is of the correct form, ;TI"Rgoing by the regular expression provided. You can require that the attribute ;TI"$matches the regular expression:;To:RDoc::Markup::BlankLine o:RDoc::Markup::Verbatim;	[I"'class Person < ActiveRecord::Base
;TI"j  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create
;TI"	end
;T:@format0o;
;	[I"RAlternatively, you can require that the specified attribute does _not_ match ;TI"the regular expression:;T@o;;	[I"'class Person < ActiveRecord::Base
;TI"8  validates_format_of :email, :without => /NOSPAM/
;TI"	end
;T;0o;
;	[I"LYou can also provide a proc or lambda which will determine the regular ;TI"<expression that will be used to validate the attribute.;T@o;;	[
I"'class Person < ActiveRecord::Base
;TI"F  # Admin can have number as a first letter in their screen name
;TI")  validates_format_of :screen_name,
;TI"~                      :with => lambda{ |person| person.admin? ? /\A[a-z0-9][a-z0-9_\-]*\Z/i : /\A[a-z][a-z0-9_\-]*\Z/i }
;TI"	end
;T;0o;
;	[I"UNote: use <tt>\A</tt> and <tt>\Z</tt> to match the start and end of the string, ;TI"=<tt>^</tt> and <tt>$</tt> match the start/end of a line.;T@o;
;	[I"OYou must pass either <tt>:with</tt> or <tt>:without</tt> as an option. In ;TI"Qaddition, both must be a regular expression or a proc or lambda, or else an ;TI"exception will be raised.;T@o;
;	[I"Configuration options:;To:RDoc::Markup::List:
@type:BULLET:@items[o:RDoc::Markup::ListItem:@label0;	[o;
;	[I"K<tt>:message</tt> - A custom error message (default is: "is invalid").;To;;0;	[o;
;	[I"R<tt>:allow_nil</tt> - If set to true, skips this validation if the attribute ;TI"#is +nil+ (default is +false+).;To;;0;	[o;
;	[I"J<tt>:allow_blank</tt> - If set to true, skips this validation if the ;TI"-attribute is blank (default is +false+).;To;;0;	[o;
;	[I"L<tt>:with</tt> - Regular expression that if the attribute matches will ;TI"Qresult in a successful validation. This can be provided as a proc or lambda ;TI"Breturning regular expression which will be called at runtime.;To;;0;	[o;
;	[I"Q<tt>:without</tt> - Regular expression that if the attribute does not match ;TI"Owill result in a successful validation. This can be provided as a proc or ;TI"Ilambda returning regular expression which will be called at runtime.;To;;0;	[o;
;	[I"J<tt>:on</tt> - Specifies when this validation is active. Runs in all ;TI"Pvalidation contexts by default (+nil+), other options are <tt>:create</tt> ;TI"and <tt>:update</tt>.;To;;0;	[o;
;	[	I"S<tt>:if</tt> - Specifies a method, proc or string to call to determine if the ;TI"Ivalidation should occur (e.g. <tt>:if => :allow_validation</tt>, or ;TI"Q<tt>:if => Proc.new { |user| user.signup_step > 2 }</tt>). The method, proc ;TI"Bor string should return or evaluate to a true or false value.;To;;0;	[o;
;	[	I"S<tt>:unless</tt> - Specifies a method, proc or string to call to determine if ;TI"Qthe validation should not occur (e.g. <tt>:unless => :skip_validation</tt>, ;TI"Tor <tt>:unless => Proc.new { |user| user.signup_step <= 2 }</tt>). The method, ;TI"Gproc or string should return or evaluate to a true or false value.;To;;0;	[o;
;	[I"H<tt>:strict</tt> - Specifies whether validation should be strict.  ;TI"JSee <tt>ActiveModel::Validation#validates!</tt> for more information.;T:
@fileI"+lib/active_model/validations/format.rb;T:0@omit_headings_from_table_of_contents_below000[ I"(*attr_names);T@tFI"HelperMethods;FcRDoc::NormalModule0