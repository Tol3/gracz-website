U:RDoc::AnyMethod[iI"validates_numericality_of:EFI"FActiveModel::Validations::HelperMethods#validates_numericality_of;FF:publico:RDoc::Markup::Document:@parts[o:RDoc::Markup::Paragraph;	[I"bValidates whether the value of the specified attribute is numeric by trying to convert it to ;TI"la float with Kernel.Float (if <tt>only_integer</tt> is false) or applying it to the regular expression ;TI"I<tt>/\A[\+\-]?\d+\Z/</tt> (if <tt>only_integer</tt> is set to true).;To:RDoc::Markup::BlankLine o:RDoc::Markup::Verbatim;	[I"'class Person < ActiveRecord::Base
;TI"8  validates_numericality_of :value, :on => :create
;TI"	end
;T:@format0o;
;	[I"Configuration options:;To:RDoc::Markup::List:
@type:BULLET:@items[o:RDoc::Markup::ListItem:@label0;	[o;
;	[I"P<tt>:message</tt> - A custom error message (default is: "is not a number").;To;;0;	[o;
;	[I"J<tt>:on</tt> - Specifies when this validation is active. Runs in all ;TI"Pvalidation contexts by default (+nil+), other options are <tt>:create</tt> ;TI"and <tt>:update</tt>.;To;;0;	[o;
;	[I"|<tt>:only_integer</tt> - Specifies whether the value has to be an integer, e.g. an integral value (default is +false+).;To;;0;	[o;
;	[I"M<tt>:allow_nil</tt> - Skip validation if attribute is +nil+ (default is ;TI"J+false+). Notice that for fixnum and float columns empty strings are ;TI"converted to +nil+.;To;;0;	[o;
;	[I"K<tt>:greater_than</tt> - Specifies the value must be greater than the ;TI"supplied value.;To;;0;	[o;
;	[I"N<tt>:greater_than_or_equal_to</tt> - Specifies the value must be greater ;TI"&than or equal the supplied value.;To;;0;	[o;
;	[I"R<tt>:equal_to</tt> - Specifies the value must be equal to the supplied value.;To;;0;	[o;
;	[I"N<tt>:less_than</tt> - Specifies the value must be less than the supplied ;TI"value.;To;;0;	[o;
;	[I"M<tt>:less_than_or_equal_to</tt> - Specifies the value must be less than ;TI"!or equal the supplied value.;To;;0;	[o;
;	[I"?<tt>:odd</tt> - Specifies the value must be an odd number.;To;;0;	[o;
;	[I"A<tt>:even</tt> - Specifies the value must be an even number.;To;;0;	[o;
;	[	I"L<tt>:if</tt> - Specifies a method, proc or string to call to determine ;TI"Mif the validation should occur (e.g. <tt>:if => :allow_validation</tt>, ;TI"Oor <tt>:if => Proc.new { |user| user.signup_step > 2 }</tt>). The method, ;TI"Gproc or string should return or evaluate to a true or false value.;To;;0;	[o;
;	[	I"P<tt>:unless</tt> - Specifies a method, proc or string to call to determine ;TI"Tif the validation should not occur (e.g. <tt>:unless => :skip_validation</tt>, ;TI"Tor <tt>:unless => Proc.new { |user| user.signup_step <= 2 }</tt>). The method, ;TI"Gproc or string should return or evaluate to a true or false value.;To;;0;	[o;
;	[I"H<tt>:strict</tt> - Specifies whether validation should be strict.  ;TI"JSee <tt>ActiveModel::Validation#validates!</tt> for more information.;T@o;
;	[I"eThe following checks can also be supplied with a proc or a symbol which corresponds to a method:;To;;;;[
o;;0;	[o;
;	[I"<tt>:greater_than</tt>;To;;0;	[o;
;	[I"'<tt>:greater_than_or_equal_to</tt>;To;;0;	[o;
;	[I"<tt>:equal_to</tt>;To;;0;	[o;
;	[I"<tt>:less_than</tt>;To;;0;	[o;
;	[I"$<tt>:less_than_or_equal_to</tt>;T@o;
;	[I"For example:;T@o;;	[	I"'class Person < ActiveRecord::Base
;TI"[  validates_numericality_of :width, :less_than => Proc.new { |person| person.height }
;TI"J  validates_numericality_of :width, :greater_than => :minimum_weight
;TI"end;T;0:
@fileI"1lib/active_model/validations/numericality.rb;T:0@omit_headings_from_table_of_contents_below000[ I"(*attr_names);T@�FI"HelperMethods;FcRDoc::NormalModule0