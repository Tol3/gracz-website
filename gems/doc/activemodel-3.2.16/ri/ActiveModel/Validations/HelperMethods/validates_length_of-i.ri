U:RDoc::AnyMethod[iI"validates_length_of:EFI"@ActiveModel::Validations::HelperMethods#validates_length_of;FF:publico:RDoc::Markup::Document:@parts[
o:RDoc::Markup::Paragraph;	[I"VValidates that the specified attribute matches the length restrictions supplied. ;TI"+Only one option can be used at a time:;To:RDoc::Markup::BlankLine o:RDoc::Markup::Verbatim;	[I"'class Person < ActiveRecord::Base
;TI"7  validates_length_of :first_name, :maximum => 30
;TI"d  validates_length_of :last_name, :maximum => 30, :message => "less than 30 if you don't mind"
;TI"B  validates_length_of :fax, :in => 7..32, :allow_nil => true
;TI"F  validates_length_of :phone, :in => 7..32, :allow_blank => true
;TI"{  validates_length_of :user_name, :within => 6..20, :too_long => "pick a shorter name", :too_short => "pick a longer name"
;TI"h  validates_length_of :zip_code, :minimum => 5, :too_short => "please enter at least 5 characters"
;TI"v  validates_length_of :smurf_leader, :is => 4, :message => "papa is spelled with 4 characters... don't play me."
;TI"l  validates_length_of :essay, :minimum => 100, :too_short => "Your essay must be at least 100 words.",
;TI"J                      :tokenizer => lambda { |str| str.scan(/\w+/) }
;TI"	end
;T:@format0o;
;	[I"Configuration options:;To:RDoc::Markup::List:
@type:BULLET:@items[o:RDoc::Markup::ListItem:@label0;	[o;
;	[I";<tt>:minimum</tt> - The minimum size of the attribute.;To;;0;	[o;
;	[I";<tt>:maximum</tt> - The maximum size of the attribute.;To;;0;	[o;
;	[I"4<tt>:is</tt> - The exact size of the attribute.;To;;0;	[o;
;	[I"O<tt>:within</tt> - A range specifying the minimum and maximum size of the ;TI"attribute.;To;;0;	[o;
;	[I"=<tt>:in</tt> - A synonym(or alias) for <tt>:within</tt>.;To;;0;	[o;
;	[I"C<tt>:allow_nil</tt> - Attribute may be +nil+; skip validation.;To;;0;	[o;
;	[I"E<tt>:allow_blank</tt> - Attribute may be blank; skip validation.;To;;0;	[o;
;	[I"K<tt>:too_long</tt> - The error message if the attribute goes over the ;TI"Jmaximum (default is: "is too long (maximum is %{count} characters)").;To;;0;	[o;
;	[I"M<tt>:too_short</tt> - The error message if the attribute goes under the ;TI"Gminimum (default is: "is too short (min is %{count} characters)").;To;;0;	[o;
;	[I"Q<tt>:wrong_length</tt> - The error message if using the <tt>:is</tt> method ;TI"Kand the attribute is the wrong size (default is: "is the wrong length ;TI"&should be %{count} characters)").;To;;0;	[o;
;	[I"K<tt>:message</tt> - The error message to use for a <tt>:minimum</tt>, ;TI"O<tt>:maximum</tt>, or <tt>:is</tt> violation. An alias of the appropriate ;TI"H<tt>too_long</tt>/<tt>too_short</tt>/<tt>wrong_length</tt> message.;To;;0;	[o;
;	[I"J<tt>:on</tt> - Specifies when this validation is active. Runs in all ;TI"Pvalidation contexts by default (+nil+), other options are <tt>:create</tt> ;TI"and <tt>:update</tt>.;To;;0;	[o;
;	[	I"O<tt>:if</tt> - Specifies a method, proc or string to call to determine if ;TI"Mthe validation should occur (e.g. <tt>:if => :allow_validation</tt>, or ;TI"Q<tt>:if => Proc.new { |user| user.signup_step > 2 }</tt>). The method, proc ;TI"Bor string should return or evaluate to a true or false value.;To;;0;	[o;
;	[	I"P<tt>:unless</tt> - Specifies a method, proc or string to call to determine ;TI"Tif the validation should not occur (e.g. <tt>:unless => :skip_validation</tt>, ;TI"Lor <tt>:unless => Proc.new { |user| user.signup_step <= 2 }</tt>). The ;TI"Omethod, proc or string should return or evaluate to a true or false value.;To;;0;	[o;
;	[I"K<tt>:tokenizer</tt> - Specifies how to split up the attribute string. ;TI"P(e.g. <tt>:tokenizer => lambda {|str| str.scan(/\w+/)}</tt> to count words ;TI"uas in above example). Defaults to <tt>lambda{ |value| value.split(//) }</tt> which counts individual characters.;To;;0;	[o;
;	[I"G<tt>:strict</tt> - Specifies whether validation should be strict. ;TI"JSee <tt>ActiveModel::Validation#validates!</tt> for more information.;T:
@fileI"+lib/active_model/validations/length.rb;T:0@omit_headings_from_table_of_contents_below000[[I"validates_size_of;To;;	[ ;@;0I"(*attr_names);T@FI"HelperMethods;FcRDoc::NormalModule0