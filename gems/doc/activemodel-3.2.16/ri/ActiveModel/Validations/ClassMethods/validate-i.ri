U:RDoc::AnyMethod[iI"validate:EFI"4ActiveModel::Validations::ClassMethods#validate;FF:publico:RDoc::Markup::Document:@parts[o:RDoc::Markup::Paragraph;	[I"IAdds a validation method or block to the class. This is useful when ;TI"Hoverriding the +validate+ instance method becomes too unwieldy and ;TI"Iyou're looking for more descriptive declaration of your validations.;To:RDoc::Markup::BlankLine o;
;	[I"9This can be done with a symbol pointing to a method:;T@o:RDoc::Markup::Verbatim;	[I"class Comment
;TI"(  include ActiveModel::Validations
;TI"
;TI"!  validate :must_be_friends
;TI"
;TI"  def must_be_friends
;TI"h    errors.add(:base, "Must be friends to leave a comment") unless commenter.friend_of?(commentee)
;TI"  end
;TI"	end
;T:@format0o;
;	[I"JWith a block which is passed with the current record to be validated:;T@o;;	[I"class Comment
;TI"(  include ActiveModel::Validations
;TI"
;TI"  validate do |comment|
;TI"!    comment.must_be_friends
;TI"  end
;TI"
;TI"  def must_be_friends
;TI"h    errors.add(:base, "Must be friends to leave a comment") unless commenter.friend_of?(commentee)
;TI"  end
;TI"	end
;T;0o;
;	[I"MOr with a block where self points to the current record to be validated:;T@o;;	[I"class Comment
;TI"(  include ActiveModel::Validations
;TI"
;TI"  validate do
;TI"h    errors.add(:base, "Must be friends to leave a comment") unless commenter.friend_of?(commentee)
;TI"  end
;TI"end;T;0:
@fileI"$lib/active_model/validations.rb;T:0@omit_headings_from_table_of_contents_below000[ I"(*args, &block);T@;FI"ClassMethods;FcRDoc::NormalModule0