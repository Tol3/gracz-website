U:RDoc::AnyMethod[iI"validates:EFI"5ActiveModel::Validations::ClassMethods#validates;FF:publico:RDoc::Markup::Document:@parts[o:RDoc::Markup::Paragraph;	[	I"HThis method is a shortcut to all default validators and any custom ;TI"Fvalidator classes ending in 'Validator'. Note that Rails default ;TI"Fvalidators can be overridden inside specific classes by creating ;TI"Gcustom validator classes in their place such as PresenceValidator.;To:RDoc::Markup::BlankLine o;
;	[I"4Examples of using the default rails validators:;T@o:RDoc::Markup::Verbatim;	[I"+validates :terms, :acceptance => true
;TI"0validates :password, :confirmation => true
;TI"Gvalidates :username, :exclusion => { :in => %w(admin superuser) }
;TI"mvalidates :email, :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create }
;TI"3validates :age, :inclusion => { :in => 0..9 }
;TI":validates :first_name, :length => { :maximum => 30 }
;TI"+validates :age, :numericality => true
;TI",validates :username, :presence => true
;TI".validates :username, :uniqueness => true
;T:@format0o;
;	[I"LThe power of the +validates+ method comes when using custom validators ;TI"Band default validators in one call for a given attribute e.g.;T@o;;	[I"7class EmailValidator < ActiveModel::EachValidator
;TI"3  def validate_each(record, attribute, value)
;TI"V    record.errors.add attribute, (options[:message] || "is not an email") unless
;TI"B      value =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
;TI"  end
;TI"	end
;TI"
;TI"class Person
;TI"(  include ActiveModel::Validations
;TI"#  attr_accessor :name, :email
;TI"
;TI"_  validates :name, :presence => true, :uniqueness => true, :length => { :maximum => 100 }
;TI";  validates :email, :presence => true, :email => true
;TI"	end
;T;0o;
;	[I"GValidator classes may also exist within the class being validated ;TI"Hallowing custom modules of validators to be included as needed e.g.;T@o;;	[I"class Film
;TI"(  include ActiveModel::Validations
;TI"
;TI"9  class TitleValidator < ActiveModel::EachValidator
;TI"5    def validate_each(record, attribute, value)
;TI"Y      record.errors.add attribute, "must start with 'the'" unless value =~ /\Athe/i
;TI"    end
;TI"  end
;TI"
;TI"'  validates :name, :title => true
;TI"	end
;T;0o;
;	[I"`Additionally validator classes may be in another namespace and still used within any class.;T@o;;	[I",validates :name, :'film/title' => true
;T;0o;
;	[I"FThe validators hash can also handle regular expressions, ranges, ;TI".arrays and strings in shortcut form, e.g.;T@o;;	[I"&validates :email, :format => /@/
;TI"6validates :gender, :inclusion => %w(male female)
;TI"+validates :password, :length => 6..20
;T;0o;
;	[I"DWhen using shortcut form, ranges and arrays are passed to your ;TI"Kvalidator's initializer as +options[:in]+ while other types including ;TI"Cregular expressions and strings are passed as +options[:with]+;T@o;
;	[I"^Finally, the options +:if+, +:unless+, +:on+, +:allow_blank+, +:allow_nil+ and +:strict+ ;TI"7can be given to one specific validator, as a hash:;T@o;;	[I"]validates :password, :presence => { :if => :password_required? }, :confirmation => true
;T;0o;
;	[I" Or to all at the same time:;T@o;;	[I"^validates :password, :presence => true, :confirmation => true, :if => :password_required?;T;0:
@fileI".lib/active_model/validations/validates.rb;T:0@omit_headings_from_table_of_contents_below000[ I"(*attributes);T@fFI"ClassMethods;FcRDoc::NormalModule0