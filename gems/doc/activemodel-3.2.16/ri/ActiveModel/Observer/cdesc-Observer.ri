U:RDoc::NormalClass[iI"Observer:EFI"ActiveModel::Observer;FI"Object;To:RDoc::Markup::Document:@parts[o;;[S:RDoc::Markup::Heading:
leveli:	textI"Active Model Observers;To:RDoc::Markup::BlankLine o:RDoc::Markup::Paragraph;[
I"PObserver classes respond to life cycle callbacks to implement trigger-like ;TI"Lbehavior outside the original class. This is a great way to reduce the ;TI"Gclutter that normally comes when the model class is burdened with ;TI"Jfunctionality that doesn't pertain to the core responsibility of the ;TI"class. Example:;T@o:RDoc::Markup::Verbatim;[
I"3class CommentObserver < ActiveModel::Observer
;TI"  def after_save(comment)
;TI"Z    Notifications.comment("admin@do.com", "New comment was posted", comment).deliver
;TI"  end
;TI"	end
;T:@format0o;;[I"BThis Observer sends an email when a Comment#save is finished.;T@o;;[I"3class ContactObserver < ActiveModel::Observer
;TI"!  def after_create(contact)
;TI"3    contact.logger.info('New contact added!')
;TI"  end
;TI"
;TI""  def after_destroy(contact)
;TI"S    contact.logger.warn("Contact with an id of #{contact.id} was destroyed!")
;TI"  end
;TI"	end
;T;0o;;[I"LThis Observer uses logger to log when specific callbacks are triggered.;T@S;	;
i;I"-Observing a class that can't be inferred;T@o;;[I"NObservers will by default be mapped to the class with which they share a ;TI"Xname. So CommentObserver will be tied to observing Comment, ProductManagerObserver ;TI"Vto ProductManager, and so on. If you want to name your observer differently than ;TI"Ythe class you're interested in observing, you can use the <tt>Observer.observe</tt> ;TI"Wclass method which takes either the concrete class (Product) or a symbol for that ;TI"class (:product):;T@o;;[I"1class AuditObserver < ActiveModel::Observer
;TI"  observe :account
;TI"
;TI"!  def after_update(account)
;TI",    AuditTrail.new(account, "UPDATED")
;TI"  end
;TI"	end
;T;0o;;[I"TIf the audit observer needs to watch more than one kind of object, this can be ;TI"'specified with multiple arguments:;T@o;;[I"1class AuditObserver < ActiveModel::Observer
;TI""  observe :account, :balance
;TI"
;TI"   def after_update(record)
;TI"+    AuditTrail.new(record, "UPDATED")
;TI"  end
;TI"	end
;T;0o;;[I"WThe AuditObserver will now act on both updates to Account and Balance by treating ;TI"them both as records.;T@o;;[I"WIf you're using an Observer in a Rails application with Active Record, be sure to ;TI"Eread about the necessary configuration in the documentation for ;TI"ActiveRecord::Observer.;T:
@fileI""lib/active_model/observing.rb;T:0@omit_headings_from_table_of_contents_below0;0;0[ [ [[I"Singleton;To;;[ ;@Y;0I""lib/active_model/observing.rb;T[[I"
class;T[[:public[	[I"new;T@a[I"observe;F@a[I"observed_class;F@a[I"observed_classes;F@a[:protected[ [:private[ [I"instance;T[[;[ [;[[I"disabled_for?;F@a[;[ [[I"&ActiveSupport::DescendantsTracker;To;;[ ;@Y;0@a[U:RDoc::Context::Section[i 0o;;[ ;0;0[@YI"ActiveModel;FcRDoc::NormalModule