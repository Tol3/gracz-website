U:RDoc::NormalModule[iI"Notifications:EFI"!ActiveSupport::Notifications;F0o:RDoc::Markup::Document:@parts[o;;[=S:RDoc::Markup::Heading:
leveli:	textI"Notifications;To:RDoc::Markup::BlankLine o:RDoc::Markup::Paragraph;[I"M+ActiveSupport::Notifications+ provides an instrumentation API for Ruby.;T@S;	;
i;I"Instrumenters;T@o;;[I"0To instrument an event you just need to do:;T@o:RDoc::Markup::Verbatim;[I"RActiveSupport::Notifications.instrument("render", :extra => :information) do
;TI"  render :text => "Foo"
;TI"	end
;T:@format0o;;[I"JThat executes the block first and notifies all subscribers once done.;T@o;;[	I"TIn the example above "render" is the name of the event, and the rest is called ;TI"Qthe _payload_. The payload is a mechanism that allows instrumenters to pass ;TI"Qextra information to subscribers. Payloads consist of a hash whose contents ;TI"5are arbitrary and generally depend on the event.;T@S;	;
i;I"Subscribers;T@o;;[I"RYou can consume those events and the information they provide by registering ;TI"Ma subscriber. For instance, let's store all "render" events in an array:;T@o;;[
I"events = []
;TI"
;TI"AActiveSupport::Notifications.subscribe("render") do |*args|
;TI"@  events << ActiveSupport::Notifications::Event.new(*args)
;TI"	end
;T;0o;;[I"PThat code returns right away, you are just subscribing to "render" events. ;TI"SThe block will be called asynchronously whenever someone instruments "render":;T@o;;[I"RActiveSupport::Notifications.instrument("render", :extra => :information) do
;TI"  render :text => "Foo"
;TI"	end
;TI"
;TI"event = events.first
;TI"#event.name      # => "render"
;TI"/event.duration  # => 10 (in milliseconds)
;TI"5event.payload   # => { :extra => :information }
;T;0o;;[	I"IThe block in the +subscribe+ call gets the name of the event, start ;TI"Ptimestamp, end timestamp, a string with a unique identifier for that event ;TI"N(something like "535801666f04d0298cd6"), and a hash with the payload, in ;TI"that order.;T@o;;[I"UIf an exception happens during that particular instrumentation the payload will ;TI"Shave a key +:exception+ with an array of two elements as value: a string with ;TI"@the name of the exception class, and the exception message.;T@o;;[I"VAs the previous example depicts, the class +ActiveSupport::Notifications::Event+ ;TI"Ois able to take the arguments as they come and provide an object-oriented ;TI"interface to that data.;T@o;;[I"NYou can also subscribe to all events whose name matches a certain regexp:;T@o;;[I"AActiveSupport::Notifications.subscribe(/render/) do |*args|
;TI"  ...
;TI"	end
;T;0o;;[I"Qand even pass no argument to +subscribe+, in which case you are subscribing ;TI"to all events.;T@S;	;
i;I"Temporary Subscriptions;T@o;;[I"OSometimes you do not want to subscribe to an event for the entire life of ;TI"8the application. There are two ways to unsubscribe.;T@o;;[I"VWARNING: The instrumentation framework is designed for long-running subscribers, ;TI"Suse this feature sparingly because it wipes some internal caches and that has ;TI"&a negative impact on performance.;T@S;	;
i;I"!Subscribe While a Block Runs;T@o;;[I"LYou can subscribe to some event temporarily while some block runs. For ;TI"example, in;T@o;;[	I"&callback = lambda {|*args| ... }
;TI"OActiveSupport::Notifications.subscribed(callback, "sql.active_record") do
;TI"  ...
;TI"	end
;T;0o;;[I"Qthe callback will be called for all "sql.active_record" events instrumented ;TI"Sduring the execution of the block. The callback is unsubscribed automatically ;TI"after that.;T@S;	;
i;I"Manual Unsubscription;T@o;;[I"8The +subscribe+ method returns a subscriber object:;T@o;;[I"Nsubscriber = ActiveSupport::Notifications.subscribe("render") do |*args|
;TI"  ...
;TI"	end
;T;0o;;[I"OTo prevent that block from being called anymore, just unsubscribe passing ;TI"that reference:;T@o;;[I":ActiveSupport::Notifications.unsubscribe(subscriber)
;T;0S;	;
i;I"Default Queue;T@o;;[I"VNotifications ships with a queue implementation that consumes and publish events ;TI"Sto log subscribers in a thread. You can use any queue implementation you want.;T:
@fileI"(lib/active_support/notifications.rb;T:0@omit_headings_from_table_of_contents_below0o;;[ ;I"/lib/active_support/notifications/fanout.rb;T;0o;;[ ;I"5lib/active_support/notifications/instrumenter.rb;T;0;0;0[[
I"notifier;TI"RW;T:publicTI"(lib/active_support/notifications.rb;T[ [ [[I"
class;T[[;[[I"instrument;F@�[I"instrumenter;F@�[I"publish;F@�[I"subscribe;F@�[I"subscribed;F@�[I"unsubscribe;F@�[:protected[ [:private[ [I"instance;T[[;[ [;[ [;[ [ [U:RDoc::Context::Section[i 0o;;[ ;0;0[@�@�@�I"ActiveSupport;FcRDoc::NormalModule