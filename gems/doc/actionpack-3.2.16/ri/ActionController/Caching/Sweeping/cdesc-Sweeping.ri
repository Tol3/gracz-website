U:RDoc::NormalModule[iI"Sweeping:EFI"(ActionController::Caching::Sweeping;F0o:RDoc::Markup::Document:@parts[o;;[o:RDoc::Markup::Paragraph;[I"vSweepers are the terminators of the caching world and responsible for expiring caches when model objects change. ;TI"uThey do this by being half-observers, half-filters and implementing callbacks for both roles. A Sweeper example:;To:RDoc::Markup::BlankLine o:RDoc::Markup::Verbatim;[I"<class ListSweeper < ActionController::Caching::Sweeper
;TI"  observe List, Item
;TI"
;TI"  def after_save(record)
;TI":    list = record.is_a?(List) ? record : record.list
;TI"`    expire_page(:controller => "lists", :action => %w( show public feed ), :id => list.id)
;TI"A    expire_action(:controller => "lists", :action => "all")
;TI"s    list.shares.each { |share| expire_page(:controller => "lists", :action => "show", :id => share.url_key) }
;TI"  end
;TI"	end
;T:@format0o;	;[I"}The sweeper is assigned in the controllers that wish to have its job performed using the <tt>cache_sweeper</tt> class method:;T@o;;[	I"3class ListsController < ApplicationController
;TI"3  caches_action :index, :show, :public, :feed
;TI"I  cache_sweeper :list_sweeper, :only => [ :edit, :destroy, :share ]
;TI"	end
;T;0o;	;[I"oIn the example above, four actions are cached and three actions are responsible for expiring those caches.;T@o;	;[I"xYou can also name an explicit class in the declaration of a sweeper, which is needed if the sweeper is in a module:;T@o;;[	I"3class ListsController < ApplicationController
;TI"3  caches_action :index, :show, :public, :feed
;TI"L  cache_sweeper OpenBar::Sweeper, :only => [ :edit, :destroy, :share ]
;TI"end;T;0:
@fileI".lib/action_controller/caching/sweeping.rb;T:0@omit_headings_from_table_of_contents_below0;0;0[ [ [ [[I"
class;T[[:public[ [:protected[ [:private[ [I"instance;T[[;[ [;[ [;[ [[I"ActiveSupport::Concern;To;;[ ;@3;0I".lib/action_controller/caching/sweeping.rb;T[U:RDoc::Context::Section[i 0o;;[ ;0;0[@3I"ActionController::Caching;FcRDoc::NormalModule