U:RDoc::AnyMethod[iI"
cache:EFI"+ActionView::Helpers::CacheHelper#cache;FF:publico:RDoc::Markup::Document:@parts[o:RDoc::Markup::Paragraph;	[
I"CThis helper exposes a method for caching fragments of a view  ;TI"Drather than an entire action or page. This technique is useful ;TI"Acaching pieces like menus, lists of newstopics, static HTML ;TI"Cfragments, and so on. This method takes a block that contains ;TI"$the content you wish to cache. ;To:RDoc::Markup::BlankLine o;
;	[I"ESee ActionController::Caching::Fragments for usage instructions.;T@S:RDoc::Markup::Heading:
leveli	:	textI"Examples;To;
;	[I"BIf you want to cache a navigation menu, you can do following:;T@o:RDoc::Markup::Verbatim;	[I"<% cache do %>
;TI"(  <%= render :partial => "menu" %>
;TI"<% end %>
;T:@format0o;
;	[I"'You can also cache static content:;T@o;;	[I"<% cache do %>
;TI"5   <p>Hello users!  Welcome to our website!</p>
;TI"<% end %>
;T;0o;
;	[I"AStatic content with embedded ruby content can be cached as  ;TI"
well:;T@o;;	[
I"<% cache do %>
;TI"  Topics:
;TI"F  <%= render :partial => "topics", :collection => @topic_list %>
;TI"+  <i>Topics listed alphabetically</i>
;TI"<% end %>;T;0:
@fileI",lib/action_view/helpers/cache_helper.rb;T:0@omit_headings_from_table_of_contents_below00I" ;T[ I"'(name = {}, options = nil, &block);T@3FI"CacheHelper;FcRDoc::NormalModule0