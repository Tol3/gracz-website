U:RDoc::AnyMethod[iI"form_for:EFI"-ActionView::Helpers::FormHelper#form_for;FF:publico:RDoc::Markup::Document:@parts[zo:RDoc::Markup::Paragraph;	[I"LCreates a form and a scope around a specific model object that is used ;TI";as a base for questioning about values for the fields.;To:RDoc::Markup::BlankLine o;
;	[I"ORails provides succinct resource-oriented form generation with +form_for+ ;TI"like this:;T@o:RDoc::Markup::Verbatim;	[I"#<%= form_for @offer do |f| %>
;TI"+  <%= f.label :version, 'Version' %>:
;TI"*  <%= f.text_field :version %><br />
;TI")  <%= f.label :author, 'Author' %>:
;TI")  <%= f.text_field :author %><br />
;TI"  <%= f.submit %>
;TI"<% end %>
;T:@format0o;
;	[	I"DThere, +form_for+ is able to generate the rest of RESTful form ;TI"Mparameters based on introspection on the record, but to understand what ;TI"Kit does we need to dig first into the alternative generic usage it is ;TI"based upon.;T@S:RDoc::Markup::Heading:
leveli:	textI"Generic form_for;T@o;
;	[I"GThe generic way to call +form_for+ yields a form builder around a ;TI"model:;T@o;;	[I"$<%= form_for :person do |f| %>
;TI"9  First name: <%= f.text_field :first_name %><br />
;TI"8  Last name : <%= f.text_field :last_name %><br />
;TI"7  Biography : <%= f.text_area :biography %><br />
;TI"3  Admin?    : <%= f.check_box :admin %><br />
;TI"  <%= f.submit %>
;TI"<% end %>
;T;0o;
;	[I"DThere, the argument is a symbol or string with the name of the ;TI"object the form is about.;T@o;
;	[I"MThe form builder acts as a regular form helper that somehow carries the ;TI""model. Thus, the idea is that;T@o;;	[I"%<%= f.text_field :first_name %>
;T;0o;
;	[I"gets expanded to;T@o;;	[I",<%= text_field :person, :first_name %>
;T;0o;
;	[I"0The rightmost argument to +form_for+ is an ;TI"optional hash of options:;T@o:RDoc::Markup::List:
@type:BULLET:@items[o:RDoc::Markup::ListItem:@label0;	[o;
;	[I"I<tt>:url</tt> - The URL the form is submitted to. It takes the same ;TI"Kfields you pass to +url_for+ or +link_to+. In particular you may pass ;TI"Ihere a named route directly as well. Defaults to the current action.;To;;0;	[o;
;	[I"M<tt>:namespace</tt> - A namespace for your form to ensure uniqueness of ;TI"Nid attributes on form elements. The namespace attribute will be prefixed ;TI".with underscore on the generated HTML id.;To;;0;	[o;
;	[I"@<tt>:html</tt> - Optional HTML attributes for the form tag.;T@o;
;	[I"MAlso note that +form_for+ doesn't create an exclusive scope. It's still ;TI"Ipossible to use both the stand-alone FormHelper methods and methods ;TI"%from FormTagHelper. For example:;T@o;;	[I"$<%= form_for @person do |f| %>
;TI"3  First name: <%= f.text_field :first_name %>
;TI"2  Last name : <%= f.text_field :last_name %>
;TI"8  Biography : <%= text_area :person, :biography %>
;TI"P  Admin?    : <%= check_box_tag "person[admin]", @person.company.admin? %>
;TI"  <%= f.submit %>
;TI"<% end %>
;T;0o;
;	[I"MThis also works for the methods in FormOptionHelper and DateHelper that ;TI"7are designed to work with an object as base, like ;TI"GFormOptionHelper#collection_select and DateHelper#datetime_select.;T@S;;i;I"Resource-oriented style;T@o;
;	[	I"JAs we said above, in addition to manually configuring the +form_for+ ;TI"Mcall, you can rely on automated resource identification, which will use ;TI"Dthe conventions and named routes of that approach. This is the ;TI".preferred way to use +form_for+ nowadays.;T@o;
;	[I"JFor example, if <tt>@post</tt> is an existing record you want to edit;T@o;;	[I""<%= form_for @post do |f| %>
;TI"  ...
;TI"<% end %>
;T;0o;
;	[I"%is equivalent to something like:;T@o;;	[I"�<%= form_for @post, :as => :post, :url => post_path(@post), :method => :put, :html => { :class => "edit_post", :id => "edit_post_45" } do |f| %>
;TI"  ...
;TI"<% end %>
;T;0o;
;	[I"And for new records;T@o;;	[I"&<%= form_for(Post.new) do |f| %>
;TI"  ...
;TI"<% end %>
;T;0o;
;	[I"%is equivalent to something like:;T@o;;	[I"z<%= form_for @post, :as => :post, :url => posts_path, :html => { :class => "new_post", :id => "new_post" } do |f| %>
;TI"  ...
;TI"<% end %>
;T;0o;
;	[I"BYou can also overwrite the individual conventions, like this:;T@o;;	[I"=<%= form_for(@post, :url => super_posts_path) do |f| %>
;TI"  ...
;TI"<% end %>
;T;0o;
;	[I"3You can also set the answer format, like this:;T@o;;	[I"5<%= form_for(@post, :format => :json) do |f| %>
;TI"  ...
;TI"<% end %>
;T;0o;
;	[I"GIf you have an object that needs to be represented as a different ;TI"4parameter, like a Person that acts as a Client:;T@o;;	[I"5<%= form_for(@person, :as => :client) do |f| %>
;TI"  ...
;TI"<% end %>
;T;0o;
;	[I"2For namespaced routes, like +admin_post_url+:;T@o;;	[I"-<%= form_for([:admin, @post]) do |f| %>
;TI"
 ...
;TI"<% end %>
;T;0o;
;	[I"VIf your resource has associations defined, for example, you want to add comments ;TI"=to the document given that the routes are set correctly:;T@o;;	[I"3<%= form_for([@document, @comment]) do |f| %>
;TI"
 ...
;TI"<% end %>
;T;0o;
;	[I"?Where <tt>@document = Document.find(params[:id])</tt> and ;TI"%<tt>@comment = Comment.new</tt>.;T@S;;i;I"Setting the method;T@o;
;	[I"JYou can force the form to use the full array of HTTP verbs by setting;T@o;;	[I"*:method => (:get|:post|:put|:delete)
;T;0o;
;	[I"jin the options hash. If the verb is not GET or POST, which are natively supported by HTML forms, the ;TI"lform will be set to POST and a hidden input called _method will carry the intended verb for the server ;TI"to interpret.;T@S;;i;I"Unobtrusive JavaScript;T@o;
;	[I"Specifying:;T@o;;	[I":remote => true
;T;0o;
;	[
I"iin the options hash creates a form that will allow the unobtrusive JavaScript drivers to modify its ;TI"kbehavior. The expected default behavior is an XMLHttpRequest in the background instead of the regular ;TI"gPOST arrangement, but ultimately the behavior is the choice of the JavaScript driver implementor. ;TI"oEven though it's using JavaScript to serialize the form elements, the form submission will work just like ;TI"fa regular submission as viewed by the receiving side (all elements available in <tt>params</tt>).;T@o;
;	[I"Example:;T@o;;	[I"4<%= form_for(@post, :remote => true) do |f| %>
;TI"  ...
;TI"<% end %>
;T;0o;
;	[I"*The HTML generated for this would be:;T@o;;	[I"M<form action='http://www.example.com' method='post' data-remote='true'>
;TI"7  <div style='margin:0;padding:0;display:inline'>
;TI"<    <input name='_method' type='hidden' value='put' />
;TI"  </div>
;TI"  ...
;TI"</form>
;T;0S;;i;I"Removing hidden model id's;T@o;
;	[	I"\The form_for method automatically includes the model id as a hidden field in the form. ;TI"^This is used to maintain the correlation between the form data and its associated model. ;TI"ZSome ORM systems do not use IDs on nested models so in this case you want to be able ;TI"to disable the hidden id.;T@o;
;	[I"eIn the following example the Post model has many Comments stored within it in a NoSQL database, ;TI"/thus there is no primary key for comments.;T@o;
;	[I"Example:;T@o;;	[
I"#<%= form_for(@post) do |f| %>
;TI"C  <% f.fields_for(:comments, :include_id => false) do |cf| %>
;TI"    ...
;TI"  <% end %>
;TI"<% end %>
;T;0S;;i;I"Customized form builders;T@o;
;	[	I"MYou can also build forms using a customized FormBuilder class. Subclass ;TI"IFormBuilder and override or define some more helpers, then use your ;TI"Acustom builder. For example, let's say you made a helper to ;TI"-automatically add labels to form inputs.;T@o;;	[I"g<%= form_for @person, :url => { :action => "create" }, :builder => LabellingFormBuilder do |f| %>
;TI"'  <%= f.text_field :first_name %>
;TI"&  <%= f.text_field :last_name %>
;TI"%  <%= f.text_area :biography %>
;TI"!  <%= f.check_box :admin %>
;TI"  <%= f.submit %>
;TI"<% end %>
;T;0o;
;	[I"#In this case, if you use this:;T@o;;	[I"<%= render f %>
;T;0o;
;	[I"LThe rendered template is <tt>people/_labelling_form</tt> and the local ;TI"5variable referencing the form builder is called ;TI"<tt>labelling_form</tt>.;T@o;
;	[I"KThe custom FormBuilder class is automatically merged with the options ;TI"=of a nested fields_for call, unless it's explicitly set.;T@o;
;	[I"MIn many cases you will want to wrap the above in another helper, so you ;TI"+could do something like the following:;T@o;;	[	I"Cdef labelled_form_for(record_or_name_or_array, *args, &block)
;TI"'  options = args.extract_options!
;TI"m  form_for(record_or_name_or_array, *(args << options.merge(:builder => LabellingFormBuilder)), &block)
;TI"	end
;T;0o;
;	[I"LIf you don't need to attach a form to a model instance, then check out ;TI"FormTagHelper#form_tag.;T@S;;i;I"Form to external resources;T@o;
;	[I"vWhen you build forms to external resources sometimes you need to set an authenticity token or just render a form ;TI"swithout it, for example when you submit data to a payment gateway number and types of fields could be limited.;T@o;
;	[I"\To set an authenticity token you need to pass an <tt>:authenticity_token</tt> parameter;T@o;;	[I"a<%= form_for @invoice, :url => external_url, :authenticity_token => 'external_token' do |f|
;TI"  ...
;TI"<% end %>
;T;0o;
;	[I"bIf you don't want to an authenticity token field be rendered at all just pass <tt>false</tt>:;T@o;;	[I"V<%= form_for @invoice, :url => external_url, :authenticity_token => false do |f|
;TI"  ...
;TI"<% end %>;T;0:
@fileI"+lib/action_view/helpers/form_helper.rb;T:0@omit_headings_from_table_of_contents_below000[ I"#(record, options = {}, &block);T@IFI"FormHelper;FcRDoc::NormalModule0