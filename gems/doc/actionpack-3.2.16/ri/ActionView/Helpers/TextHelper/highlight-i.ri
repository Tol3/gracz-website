U:RDoc::AnyMethod[iI"highlight:EFI".ActionView::Helpers::TextHelper#highlight;FF:publico:RDoc::Markup::Document:@parts[o:RDoc::Markup::Paragraph;	[	I"PHighlights one or more +phrases+ everywhere in +text+ by inserting it into ;TI"ia <tt>:highlighter</tt> string. The highlighter can be specialized by passing <tt>:highlighter</tt> ;TI"Was a single-quoted string with \1 where the phrase is to be inserted (defaults to ;TI"-'<strong class="highlight">\1</strong>');To:RDoc::Markup::BlankLine S:RDoc::Markup::Heading:
leveli	:	textI"Examples;To:RDoc::Markup::Verbatim;	[I"3highlight('You searched for: rails', 'rails')
;TI"E# => You searched for: <strong class="highlight">rails</strong>
;TI"
;TI"Chighlight('You searched for: ruby, rails, dhh', 'actionpack')
;TI"-# => You searched for: ruby, rails, dhh
;TI"
;TI"[highlight('You searched for: rails', ['for', 'rails'], :highlighter => '<em>\1</em>')
;TI"4# => You searched <em>for</em>: <em>rails</em>
;TI"
;TI"chighlight('You searched for: rails', 'rails', :highlighter => '<a href="search?q=\1">\1</a>')
;TI"?# => You searched for: <a href="search?q=rails">rails</a>
;T:@format0o;
;	[I"LYou can still use <tt>highlight</tt> with the old API that accepts the ;TI"3+highlighter+ as its optional third parameter:;To;;	[I"�highlight('You searched for: rails', 'rails', '<a href="search?q=\1">\1</a>')     # => You searched for: <a href="search?q=rails">rails</a>;T;0:
@fileI"+lib/action_view/helpers/text_helper.rb;T:0@omit_headings_from_table_of_contents_below000[ I"(text, phrases, *args);T@(FI"TextHelper;FcRDoc::NormalModule0