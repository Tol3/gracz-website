U:RDoc::AnyMethod[iI"mail_to:EFI"+ActionView::Helpers::UrlHelper#mail_to;FF:publico:RDoc::Markup::Document:@parts[o:RDoc::Markup::Paragraph;	[I"JCreates a mailto link tag to the specified +email_address+, which is ;TI"Nalso used as the name of the link unless +name+ is specified. Additional ;TI"BHTML attributes for the link can be passed in +html_options+.;To:RDoc::Markup::BlankLine o;
;	[I"R+mail_to+ has several methods for hindering email harvesters and customizing ;TI"@the email itself by passing special keys to +html_options+.;T@S:RDoc::Markup::Heading:
leveli	:	textI"Options;To:RDoc::Markup::List:
@type:BULLET:@items[o:RDoc::Markup::ListItem:@label0;	[o;
;	[
I"P<tt>:encode</tt> - This key will accept the strings "javascript" or "hex". ;TI"RPassing "javascript" will dynamically create and encode the mailto link then ;TI"Meval it into the DOM of the page. This method will not show the link on ;TI"Jthe page if the user has JavaScript disabled. Passing "hex" will hex ;TI"Bencode the +email_address+ before outputting the mailto link.;To;;0;	[o;
;	[	I"E<tt>:replace_at</tt> - When the link +name+ isn't provided, the ;TI"L+email_address+ is used for the link label. You can use this option to ;TI"Nobfuscate the +email_address+ by substituting the @ sign with the string ;TI"given as the value.;To;;0;	[o;
;	[	I"F<tt>:replace_dot</tt> - When the link +name+ isn't provided, the ;TI"L+email_address+ is used for the link label. You can use this option to ;TI"Oobfuscate the +email_address+ by substituting the . in the email with the ;TI"string given as the value.;To;;0;	[o;
;	[I"><tt>:subject</tt> - Preset the subject line of the email.;To;;0;	[o;
;	[I"3<tt>:body</tt> - Preset the body of the email.;To;;0;	[o;
;	[I"C<tt>:cc</tt> - Carbon Copy additional recipients on the email.;To;;0;	[o;
;	[I"J<tt>:bcc</tt> - Blind Carbon Copy additional recipients on the email.;T@S;;i	;I"Examples;To:RDoc::Markup::Verbatim;	[I"mail_to "me@domain.com"
;TI";# => <a href="mailto:me@domain.com">me@domain.com</a>
;TI"
;TI"Bmail_to "me@domain.com", "My email", :encode => "javascript"
;TI"d# => <script type="text/javascript">eval(decodeURIComponent('%64%6f%63...%27%29%3b'))</script>
;TI"
;TI";mail_to "me@domain.com", "My email", :encode => "hex"
;TI"L# => <a href="mailto:%6d%65@%64%6f%6d%61%69%6e.%63%6f%6d">My email</a>
;TI"
;TI"email_to "me@domain.com", nil, :replace_at => "_at_", :replace_dot => "_dot_", :class => "email"
;TI"P# => <a href="mailto:me@domain.com" class="email">me_at_domain_dot_com</a>
;TI"
;TI"Imail_to "me@domain.com", "My email", :cc => "ccaddress@domain.com",
;TI"5         :subject => "This is an example email"
;TI"v# => <a href="mailto:me@domain.com?cc=ccaddress@domain.com&subject=This%20is%20an%20example%20email">My email</a>;T:@format0:
@fileI"*lib/action_view/helpers/url_helper.rb;T:0@omit_headings_from_table_of_contents_below000[ I"3(email_address, name = nil, html_options = {});T@YFI"UrlHelper;FcRDoc::NormalModule0