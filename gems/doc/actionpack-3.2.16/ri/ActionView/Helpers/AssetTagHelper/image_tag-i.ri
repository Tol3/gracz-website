U:RDoc::AnyMethod[iI"image_tag:EFI"2ActionView::Helpers::AssetTagHelper#image_tag;FF:publico:RDoc::Markup::Document:@parts[o:RDoc::Markup::Paragraph;	[I"LReturns an html image tag for the +source+. The +source+ can be a full ;TI"@path or a file that exists in your public images directory.;To:RDoc::Markup::BlankLine S:RDoc::Markup::Heading:
leveli	:	textI"Options;To;
;	[I"MYou can add HTML attributes using the +options+. The +options+ supports ;TI";three additional keys for convenience and conformance:;T@o:RDoc::Markup::List:
@type:BULLET:@items[o:RDoc::Markup::ListItem:@label0;	[o;
;	[I"I<tt>:alt</tt>  - If no alt text is given, the file name part of the ;TI"=+source+ is used (capitalized and without the extension);To;;0;	[o;
;	[I"I<tt>:size</tt> - Supplied as "{Width}x{Height}", so "30x45" becomes ;TI"Gwidth="30" and height="45". <tt>:size</tt> will be ignored if the ;TI"(value is not in the correct format.;To;;0;	[o;
;	[I"R<tt>:mouseover</tt> - Set an alternate image to be used when the onmouseover ;TI"Levent is fired, and sets the original image to be replaced onmouseout. ;TI"RThis can be used to implement an easy image toggle that fires on onmouseover.;T@S;;i	;I"Examples;To:RDoc::Markup::Verbatim;	[I"image_tag("icon")  # =>
;TI"-  <img src="/images/icon" alt="Icon" />
;TI"!image_tag("icon.png")  # =>
;TI"1  <img src="/images/icon.png" alt="Icon" />
;TI"Iimage_tag("icon.png", :size => "16x10", :alt => "Edit Entry")  # =>
;TI"N  <img src="/images/icon.png" width="16" height="10" alt="Edit Entry" />
;TI":image_tag("/icons/icon.gif", :size => "16x16")  # =>
;TI"G  <img src="/icons/icon.gif" width="16" height="16" alt="Icon" />
;TI"Himage_tag("/icons/icon.gif", :height => '32', :width => '32') # =>
;TI"G  <img alt="Icon" height="32" src="/icons/icon.gif" width="32" />
;TI">image_tag("/icons/icon.gif", :class => "menu_icon") # =>
;TI"B  <img alt="Icon" class="menu_icon" src="/icons/icon.gif" />
;TI"Iimage_tag("mouse.png", :mouseover => "/images/mouse_over.png") # =>
;TI"�  <img src="/images/mouse.png" onmouseover="this.src='/images/mouse_over.png'" onmouseout="this.src='/images/mouse.png'" alt="Mouse" />
;TI"Mimage_tag("mouse.png", :mouseover => image_path("mouse_over.png")) # =>
;TI"�  <img src="/images/mouse.png" onmouseover="this.src='/images/mouse_over.png'" onmouseout="this.src='/images/mouse.png'" alt="Mouse" />;T:@format0:
@fileI"0lib/action_view/helpers/asset_tag_helper.rb;T:0@omit_headings_from_table_of_contents_below000[ I"(source, options = {});T@@FI"AssetTagHelper;FcRDoc::NormalModule0