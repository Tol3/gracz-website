U:RDoc::AnyMethod[iI"delete_all:EFI"&ActiveRecord::Relation#delete_all;FF:publico:RDoc::Markup::Document:@parts[o:RDoc::Markup::Paragraph;	[
I"fDeletes the records matching +conditions+ without instantiating the records first, and hence not ;TI"ecalling the +destroy+ method nor invoking callbacks. This is a single SQL DELETE statement that ;TI"fgoes straight to the database, much more efficient than +destroy_all+. Be careful with relations ;TI"fthough, in particular <tt>:dependent</tt> rules defined on associations are not honored. Returns ;TI"!the number of rows affected.;To:RDoc::Markup::BlankLine S:RDoc::Markup::Heading:
leveli	:	textI"Parameters;T@o:RDoc::Markup::List:
@type:BULLET:@items[o:RDoc::Markup::ListItem:@label0;	[o;
;	[I"P+conditions+ - Conditions are specified the same way as with +find+ method.;T@S;;i	;I"Example;T@o:RDoc::Markup::Verbatim;	[I"XPost.delete_all("person_id = 5 AND (category = 'Something' OR category = 'Else')")
;TI"cPost.delete_all(["person_id = ? AND (category = ? OR category = ?)", 5, 'Something', 'Else'])
;TI"VPost.where(:person_id => 5).where(:category => ['Something', 'Else']).delete_all
;T:@format0o;
;	[I"VBoth calls delete the affected posts all at once with a single DELETE statement. ;TI"UIf you need to destroy dependent associations or call your <tt>before_*</tt> or ;TI"E+after_destroy+ callbacks, use the +destroy_all+ method instead.;T:
@fileI""lib/active_record/relation.rb;T:0@omit_headings_from_table_of_contents_below000[ I"(conditions = nil);T@(FI"Relation;FcRDoc::NormalClass0