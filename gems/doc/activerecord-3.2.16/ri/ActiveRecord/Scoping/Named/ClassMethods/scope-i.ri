U:RDoc::AnyMethod[iI"
scope:EFI"5ActiveRecord::Scoping::Named::ClassMethods#scope;FF:publico:RDoc::Markup::Document:@parts[0o:RDoc::Markup::Paragraph;	[I"sAdds a class method for retrieving and querying objects. A \scope represents a narrowing of a database query, ;TI"_such as <tt>where(:color => :red).select('shirts.*').includes(:washing_instructions)</tt>.;To:RDoc::Markup::BlankLine o:RDoc::Markup::Verbatim;	[	I"&class Shirt < ActiveRecord::Base
;TI"*  scope :red, where(:color => 'red')
;TI"r  scope :dry_clean_only, joins(:washing_instructions).where('washing_instructions.dry_clean_only = ?', true)
;TI"	end
;T:@format0o;
;	[I"kThe above calls to <tt>scope</tt> define class methods Shirt.red and Shirt.dry_clean_only. Shirt.red, ;TI"Kin effect, represents the query <tt>Shirt.where(:color => 'red')</tt>.;T@o;
;	[I"TNote that this is simply 'syntactic sugar' for defining an actual class method:;T@o;;	[
I"&class Shirt < ActiveRecord::Base
;TI"  def self.red
;TI"     where(:color => 'red')
;TI"  end
;TI"	end
;T;0o;
;	[I"dUnlike <tt>Shirt.find(...)</tt>, however, the object returned by Shirt.red is not an Array; it ;TI"dresembles the association object constructed by a <tt>has_many</tt> declaration. For instance, ;TI"tyou can invoke <tt>Shirt.red.first</tt>, <tt>Shirt.red.count</tt>, <tt>Shirt.red.where(:size => 'small')</tt>. ;TI"kAlso, just as with the association objects, named \scopes act like an Array, implementing Enumerable; ;TI"l<tt>Shirt.red.each(&block)</tt>, <tt>Shirt.red.first</tt>, and <tt>Shirt.red.inject(memo, &block)</tt> ;TI"4all behave as if Shirt.red really was an Array.;T@o;
;	[
I"fThese named \scopes are composable. For instance, <tt>Shirt.red.dry_clean_only</tt> will produce ;TI"6all shirts that are both red and dry clean only. ;TI"nNested finds and calculations also work with these compositions: <tt>Shirt.red.dry_clean_only.count</tt> ;TI"Treturns the number of garments for which these criteria obtain. Similarly with ;TI"><tt>Shirt.red.dry_clean_only.average(:thread_count)</tt>.;T@o;
;	[I"`All \scopes are available as class methods on the ActiveRecord::Base descendant upon which ;TI"athe \scopes were defined. But they are also available to <tt>has_many</tt> associations. If,;T@o;;	[I"'class Person < ActiveRecord::Base
;TI"  has_many :shirts
;TI"	end
;T;0o;
;	[I"]then <tt>elton.shirts.red.dry_clean_only</tt> will return all of Elton's red, dry clean ;TI"only shirts.;T@o;
;	[I"*Named \scopes can also be procedural:;T@o;;	[I"&class Shirt < ActiveRecord::Base
;TI"A  scope :colored, lambda { |color| where(:color => color) }
;TI"	end
;T;0o;
;	[I"KIn this example, <tt>Shirt.colored('puce')</tt> finds all puce shirts.;T@o;
;	[I"8On Ruby 1.9 you can use the 'stabby lambda' syntax:;T@o;;	[I":scope :colored, ->(color) { where(:color => color) }
;T;0o;
;	[I"_Note that scopes defined with \scope will be evaluated when they are defined, rather than ;TI"Gwhen they are used. For example, the following would be incorrect:;T@o;;	[I"%class Post < ActiveRecord::Base
;TI"H  scope :recent, where('published_at >= ?', Time.current - 1.week)
;TI"	end
;T;0o;
;	[	I"cThe example above would be 'frozen' to the <tt>Time.current</tt> value when the <tt>Post</tt> ;TI"]class was defined, and so the resultant SQL query would always be the same. The correct ;TI"Vway to do this would be via a lambda, which will re-evaluate the scope each time ;TI"it is called:;T@o;;	[I"%class Post < ActiveRecord::Base
;TI"S  scope :recent, lambda { where('published_at >= ?', Time.current - 1.week) }
;TI"	end
;T;0o;
;	[I"YNamed \scopes can also have extensions, just as with <tt>has_many</tt> declarations:;T@o;;	[I"&class Shirt < ActiveRecord::Base
;TI"-  scope :red, where(:color => 'red') do
;TI"    def dom_id
;TI"      'red_shirts'
;TI"    end
;TI"  end
;TI"	end
;T;0o;
;	[I">Scopes can also be used while creating/building a record.;T@o;;	[I"(class Article < ActiveRecord::Base
;TI"3  scope :published, where(:published => true)
;TI"	end
;TI"
;TI"2Article.published.new.published    # => true
;TI"2Article.published.create.published # => true
;T;0o;
;	[I"=Class methods on your model are automatically available ;TI"-on scopes. Assuming the following setup:;T@o;;	[I"(class Article < ActiveRecord::Base
;TI"3  scope :published, where(:published => true)
;TI"1  scope :featured, where(:featured => true)
;TI"
;TI"  def self.latest_article
;TI"*    order('published_at desc').first
;TI"  end
;TI"
;TI"  def self.titles
;TI"    pluck(:title)
;TI"  end
;TI"	end
;T;0o;
;	[I"/We are able to call the methods like this:;T@o;;	[I"/Article.published.featured.latest_article
;TI"Article.featured.titles;T;0:
@fileI"'lib/active_record/scoping/named.rb;T:0@omit_headings_from_table_of_contents_below000[ I"(name, scope_options = {});T@�FI"ClassMethods;FcRDoc::NormalModule0