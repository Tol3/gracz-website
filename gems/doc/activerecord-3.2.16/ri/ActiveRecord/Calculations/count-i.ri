U:RDoc::AnyMethod[iI"
count:EFI"%ActiveRecord::Calculations#count;FF:publico:RDoc::Markup::Document:@parts[o:RDoc::Markup::Paragraph;	[I"5Count operates using three different approaches.;To:RDoc::Markup::BlankLine o:RDoc::Markup::List:
@type:BULLET:@items[o:RDoc::Markup::ListItem:@label0;	[o;
;	[I"mCount all: By not passing any parameters to count, it will return a count of all the rows for the model.;To;;0;	[o;
;	[I"^Count using column: By passing a column name to count, it will return a count of all the ;TI"5rows for the model with supplied column present.;To;;0;	[o;
;	[I"MCount using options will find the row count matched by the options used.;T@o;
;	[I"lThe third approach, count using options, accepts an option hash as the only parameter. The options are:;T@o;;;;[o;;0;	[o;
;	[I"f<tt>:conditions</tt>: An SQL fragment like "administrator = 1" or [ "user_name = ?", username ]. ;TI"7See conditions in the intro to ActiveRecord::Base.;To;;0;	[o;
;	[
I"u<tt>:joins</tt>: Either an SQL fragment for additional joins like "LEFT JOIN comments ON comments.post_id = id" ;TI"n(rarely needed) or named associations in the same form used for the <tt>:include</tt> option, which will ;TI"bperform an INNER JOIN on the associated table(s). If the value is a string, then the records ;TI"owill be returned read-only since they will have attributes that do not correspond to the table's columns. ;TI"2Pass <tt>:readonly => false</tt> to override.;To;;0;	[o;
;	[	I"c<tt>:include</tt>: Named associations that should be loaded alongside using LEFT OUTER JOINs. ;TI"cThe symbols named refer to already defined associations. When using named associations, count ;TI"Ireturns the number of DISTINCT items for the model you're counting. ;TI"*See eager loading under Associations.;To;;0;	[o;
;	[I"q<tt>:order</tt>: An SQL fragment like "created_at DESC, name" (really only used with GROUP BY calculations).;To;;0;	[o;
;	[I"l<tt>:group</tt>: An attribute name by which the result should be grouped. Uses the GROUP BY SQL-clause.;To;;0;	[o;
;	[I"j<tt>:select</tt>: By default, this is * as in SELECT * FROM, but can be changed if you, for example, ;TI":want to do a join but not include the joined columns.;To;;0;	[o;
;	[I"W<tt>:distinct</tt>: Set this to true to make this a distinct calculation, such as ;TI"(SELECT COUNT(DISTINCT posts.id) ...;To;;0;	[o;
;	[I"`<tt>:from</tt> - By default, this is the table name of the class, but can be changed to an ;TI"@alternate table name (or even the name of a database view).;T@o;
;	[I"Examples for counting all:;To:RDoc::Markup::Verbatim;	[I"BPerson.count         # returns the total count of all people
;T:@format0o;
;	[I"%Examples for counting by column:;To;;	[I"bPerson.count(:age)  # returns the total count of all people whose age is present in database
;T;0o;
;	[I"%Examples for count with options:;To;;	[I"-Person.count(:conditions => "age > 26")
;TI"
;TI"\# because of the named association, it finds the DISTINCT count using LEFT OUTER JOIN.
;TI"VPerson.count(:conditions => "age > 26 AND job.salary > 60000", :include => :job)
;TI"
;TI"C# finds the number of rows matching the conditions and joins.
;TI"DPerson.count(:conditions => "age > 26 AND job.salary > 60000",
;TI"L             :joins => "LEFT JOIN jobs on jobs.person_id = person.id")
;TI"
;TI"JPerson.count('id', :conditions => "age > 26") # Performs a COUNT(id)
;TI"dPerson.count(:all, :conditions => "age > 26") # Performs a COUNT(*) (:all is an alias for '*')
;T;0o;
;	[I"iNote: <tt>Person.count(:all)</tt> will not work because it will use <tt>:all</tt> as the condition. ;TI"Use Person.count instead.;T:
@fileI"/lib/active_record/relation/calculations.rb;T:0@omit_headings_from_table_of_contents_below000[ I"&(column_name = nil, options = {});T@yFI"Calculations;FcRDoc::NormalModule0