U:RDoc::AnyMethod[iI"select:EFI"&ActiveRecord::QueryMethods#select;FF:publico:RDoc::Markup::Document:@parts[o:RDoc::Markup::Paragraph;	[I"Works in two unique ways.;To:RDoc::Markup::BlankLine o;
;	[I"CFirst: takes a block so it can be used just like Array#select.;T@o:RDoc::Markup::Verbatim;	[I"2Model.scoped.select { |m| m.field == value }
;T:@format0o;
;	[I"JThis will build an array of objects from the database for the scope, ;TI"Qconverting them into an array and iterating through them using Array#select.;T@o;
;	[I"NSecond: Modifies the SELECT statement for the query so that only certain ;TI"fields are retrieved:;T@o;;	[I">> Model.select(:field)
;TI"=> [#<Model field:value>]
;T;0o;
;	[I"MAlthough in the above example it looks as though this method returns an ;TI"Karray, it actually returns a relation object and can have other query ;TI"Umethods appended to it, such as the other methods in ActiveRecord::QueryMethods.;T@o;
;	[I"?The argument to the method can also be an array of fields.;T@o;;	[I"<>> Model.select([:field, :other_field, :and_one_more])
;TI"O=> [#<Model field: "value", other_field: "value", and_one_more: "value">]
;T;0o;
;	[I"BAny attributes that do not have fields retrieved by a select ;TI"gwill raise a ActiveModel::MissingAttributeError when the getter method for that attribute is used:;T@o;;	[I"/>> Model.select(:field).first.other_field
;TI"J=> ActiveModel::MissingAttributeError: missing attribute: other_field;T;0:
@fileI"0lib/active_record/relation/query_methods.rb;T:0@omit_headings_from_table_of_contents_below000[ I"(value = Proc.new);T@5FI"QueryMethods;FcRDoc::NormalModule0