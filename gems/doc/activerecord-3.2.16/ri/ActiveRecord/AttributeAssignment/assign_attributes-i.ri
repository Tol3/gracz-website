U:RDoc::AnyMethod[iI"assign_attributes:EFI"8ActiveRecord::AttributeAssignment#assign_attributes;FF:publico:RDoc::Markup::Document:@parts[
o:RDoc::Markup::Paragraph;	[	I"KAllows you to set all the attributes for a particular mass-assignment ;TI"Isecurity role by passing in a hash of attributes with keys matching ;TI"Mthe attribute names (which again matches the column names) and the role ;TI"name using the :as option.;To:RDoc::Markup::BlankLine o;
;	[I"TTo bypass mass-assignment security you can use the :without_protection => true ;TI"option.;T@o:RDoc::Markup::Verbatim;	[I"%class User < ActiveRecord::Base
;TI"  attr_accessible :name
;TI"7  attr_accessible :name, :is_admin, :as => :admin
;TI"	end
;TI"
;TI"user = User.new
;TI"Duser.assign_attributes({ :name => 'Josh', :is_admin => true })
;TI"!user.name       # => "Josh"
;TI" user.is_admin?  # => false
;TI"
;TI"user = User.new
;TI"Suser.assign_attributes({ :name => 'Josh', :is_admin => true }, :as => :admin)
;TI"!user.name       # => "Josh"
;TI"user.is_admin?  # => true
;TI"
;TI"user = User.new
;TI"auser.assign_attributes({ :name => 'Josh', :is_admin => true }, :without_protection => true)
;TI"!user.name       # => "Josh"
;TI"user.is_admin?  # => true;T:@format0:
@fileI".lib/active_record/attribute_assignment.rb;T:0@omit_headings_from_table_of_contents_below000[ I"#(new_attributes, options = {});T@+FI"AttributeAssignment;FcRDoc::NormalModule0