U:RDoc::AnyMethod[iI"find_in_batches:EFI"*ActiveRecord::Batches#find_in_batches;FF:publico:RDoc::Markup::Document:@parts[o:RDoc::Markup::Paragraph;	[I"JYields each batch of records that was found by the find +options+ as ;TI"Ian array. The size of each batch is set by the <tt>:batch_size</tt> ;TI"!option; the default is 1000.;To:RDoc::Markup::BlankLine o;
;	[I"DYou can control the starting point for the batch processing by ;TI"Lsupplying the <tt>:start</tt> option. This is especially useful if you ;TI"Kwant multiple workers dealing with the same processing queue. You can ;TI"Fmake worker 1 handle all the records between id 0 and 10,000 and ;TI"Lworker 2 handle from 10,000 and beyond (by setting the <tt>:start</tt> ;TI"option on that worker).;T@o;
;	[
I"FIt's not possible to set the order. That is automatically set to ;TI"Hascending on the primary key ("id ASC") to make the batch ordering ;TI"Iwork. This also mean that this method only works with integer-based ;TI"Jprimary keys. You can't set the limit either, that's used to control ;TI"the batch sizes.;T@o;
;	[I"Example:;T@o:RDoc::Markup::Verbatim;	[	I"9Person.where("age > 21").find_in_batches do |group|
;TI"B  sleep(50) # Make sure it doesn't get too crowded in there!
;TI"7  group.each { |person| person.party_all_night! }
;TI"end;T:@format0:
@fileI"*lib/active_record/relation/batches.rb;T:0@omit_headings_from_table_of_contents_below00I"records;T[ I"(options = {});T@)FI"Batches;FcRDoc::NormalModule0