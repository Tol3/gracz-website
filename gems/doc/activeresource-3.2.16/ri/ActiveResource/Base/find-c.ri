U:RDoc::AnyMethod[iI"	find:EFI"ActiveResource::Base::find;FT:publico:RDoc::Markup::Document:@parts[o:RDoc::Markup::Paragraph;	[I"YCore method for finding resources.  Used similarly to Active Record's +find+ method.;To:RDoc::Markup::BlankLine S:RDoc::Markup::Heading:
leveli	:	textI"Arguments;To;
;	[I"WThe first argument is considered to be the scope of the query.  That is, how many ;TI"Nresources are returned from the request.  It can be one of the following.;T@o:RDoc::Markup::List:
@type:BULLET:@items[	o:RDoc::Markup::ListItem:@label0;	[o;
;	[I"/<tt>:one</tt> - Returns a single resource.;To;;0;	[o;
;	[I"8<tt>:first</tt> - Returns the first resource found.;To;;0;	[o;
;	[I"6<tt>:last</tt> - Returns the last resource found.;To;;0;	[o;
;	[I"E<tt>:all</tt> - Returns every resource that matches the request.;T@S;;i	;I"Options;T@o;;;;[o;;0;	[o;
;	[I"Y<tt>:from</tt> - Sets the path or custom method that resources will be fetched from.;To;;0;	[o;
;	[I"G<tt>:params</tt> - Sets query and \prefix (nested URL) parameters.;T@S;;i	;I"Examples;To:RDoc::Markup::Verbatim;	["I"Person.find(1)
;TI"# => GET /people/1.json
;TI"
;TI"Person.find(:all)
;TI"# => GET /people.json
;TI"
;TI"7Person.find(:all, :params => { :title => "CEO" })
;TI"%# => GET /people.json?title=CEO
;TI"
;TI"-Person.find(:first, :from => :managers)
;TI"$# => GET /people/managers.json
;TI"
;TI",Person.find(:last, :from => :managers)
;TI"$# => GET /people/managers.json
;TI"
;TI"<Person.find(:all, :from => "/companies/1/people.json")
;TI"'# => GET /companies/1/people.json
;TI"
;TI")Person.find(:one, :from => :leader)
;TI""# => GET /people/leader.json
;TI"
;TI"QPerson.find(:all, :from => :developers, :params => { :language => 'ruby' })
;TI"4# => GET /people/developers.json?language=ruby
;TI"
;TI"=Person.find(:one, :from => "/companies/1/manager.json")
;TI"(# => GET /companies/1/manager.json
;TI"
;TI";StreetAddress.find(1, :params => { :person_id => 1 })
;TI"0# => GET /people/1/street_addresses/1.json
;T:@format0S;;i;I"Failure or missing data;To;;	[I"FA failure to find the requested object raises a ResourceNotFound
;TI"2exception if the find was called with an id.
;TI"FWith any other scope, find returns nil when no data is returned.
;TI"
;TI"Person.find(1)
;TI""# => raises ResourceNotFound
;TI"
;TI"Person.find(:all)
;TI"Person.find(:first)
;TI"Person.find(:last)
;TI"# => nil;T;0:
@fileI" lib/active_resource/base.rb;T:0@omit_headings_from_table_of_contents_below000[ I"(*arguments);T@iFI"	Base;FcRDoc::NormalClass0