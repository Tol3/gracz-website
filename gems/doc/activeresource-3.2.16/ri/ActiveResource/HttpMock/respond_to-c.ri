U:RDoc::AnyMethod[iI"respond_to:EFI")ActiveResource::HttpMock::respond_to;FT:publico:RDoc::Markup::Document:@parts[o:RDoc::Markup::Paragraph;	[I"fAccepts a block which declares a set of requests and responses for the HttpMock to respond to in ;TI"the following format:;To:RDoc::Markup::BlankLine o:RDoc::Markup::Verbatim;	[I"cmock.http_method(path, request_headers = {}, body = nil, status = 200, response_headers = {})
;T:@format0S:RDoc::Markup::Heading:
leveli:	textI"Example;T@o;;	[I"C@matz  = { :person => { :id => 1, :name => "Matz" } }.to_json
;TI"3ActiveResource::HttpMock.respond_to do |mock|
;TI"T  mock.post   "/people.json",   {}, @matz, 201, "Location" => "/people/1.json"
;TI"/  mock.get    "/people/1.json", {}, @matz
;TI"2  mock.put    "/people/1.json", {}, nil, 204
;TI"2  mock.delete "/people/1.json", {}, nil, 200
;TI"	end
;T;0o;
;	[I"dAlternatively, accepts a hash of <tt>{Request => Response}</tt> pairs allowing you to generate ;TI" these the following format:;T@o;;	[I"FActiveResource::Request.new(method, path, body, request_headers)
;TI"BActiveResource::Response.new(body, status, response_headers)
;T;0S;;i;I"Example;T@o;
;	[I"8Request.new(:#{method}, path, nil, request_headers);T@o;;	[I"C@matz  = { :person => { :id => 1, :name => "Matz" } }.to_json
;TI"
;TI"Vcreate_matz      = ActiveResource::Request.new(:post, '/people.json', @matz, {})
;TI"`created_response = ActiveResource::Response.new("", 201, {"Location" => "/people/1.json"})
;TI"Qget_matz         = ActiveResource::Request.new(:get, '/people/1.json', nil)
;TI"Bok_response      = ActiveResource::Response.new("", 200, {})
;TI"
;TI"Hpairs = {create_matz => created_response, get_matz => ok_response}
;TI"
;TI"0ActiveResource::HttpMock.respond_to(pairs)
;T;0o;
;	[I"hNote, by default, every time you call +respond_to+, any previous request and response pairs stored ;TI"Ein HttpMock will be deleted giving you a clean slate to work on.;T@o;
;	[I"`If you want to override this behavior, pass in +false+ as the last argument to +respond_to+;T@S;;i;I"Example;T@o;;	[I"3ActiveResource::HttpMock.respond_to do |mock|
;TI"1  mock.send(:get, "/people/1", {}, "JSON1")
;TI"	end
;TI"5ActiveResource::HttpMock.responses.length #=> 1
;TI"
;TI":ActiveResource::HttpMock.respond_to(false) do |mock|
;TI"1  mock.send(:get, "/people/2", {}, "JSON2")
;TI"	end
;TI"5ActiveResource::HttpMock.responses.length #=> 2
;T;0o;
;	[I"jThis also works with passing in generated pairs of requests and responses, again, just pass in false ;TI"as the last argument:;T@S;;i;I"Example;T@o;;	[I"3ActiveResource::HttpMock.respond_to do |mock|
;TI"1  mock.send(:get, "/people/1", {}, "JSON1")
;TI"	end
;TI"5ActiveResource::HttpMock.responses.length #=> 1
;TI"
;TI"Qget_matz         = ActiveResource::Request.new(:get, '/people/1.json', nil)
;TI"Bok_response      = ActiveResource::Response.new("", 200, {})
;TI"
;TI"'pairs = {get_matz => ok_response}
;TI"
;TI"7ActiveResource::HttpMock.respond_to(pairs, false)
;TI"5ActiveResource::HttpMock.responses.length #=> 2
;TI"
;TI"K# If you add a response with an existing request, it will be replaced
;TI"
;TI"Dfail_response      = ActiveResource::Response.new("", 404, {})
;TI")pairs = {get_matz => fail_response}
;TI"
;TI"7ActiveResource::HttpMock.respond_to(pairs, false)
;TI"4ActiveResource::HttpMock.responses.length #=> 2;T;0:
@fileI"%lib/active_resource/http_mock.rb;T:0@omit_headings_from_table_of_contents_below00I"	mock;F[ I"(*args);T@gFI"HttpMock;FcRDoc::NormalClass0